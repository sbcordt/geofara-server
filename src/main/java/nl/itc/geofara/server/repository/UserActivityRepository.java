package nl.itc.geofara.server.repository;

import nl.itc.geofara.server.domain.UserActivity;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the UserActivity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserActivityRepository extends JpaRepository<UserActivity,Long> {

    @Query("select user_activity from UserActivity user_activity where user_activity.createdBy.login = ?#{principal.username}")
    List<UserActivity> findByCreatedByIsCurrentUser();
    
}
