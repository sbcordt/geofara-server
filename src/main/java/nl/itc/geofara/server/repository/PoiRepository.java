package nl.itc.geofara.server.repository;

import nl.itc.geofara.server.domain.Poi;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the Poi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PoiRepository extends JpaRepository<Poi,Long> {

    @Query("select poi from Poi poi where poi.poiCollection.id =:id")
    List<Poi> findByCollectionId(@Param("id") Long id);
}
