package nl.itc.geofara.server.repository;

import nl.itc.geofara.server.domain.UserAction;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the UserAction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserActionRepository extends JpaRepository<UserAction,Long> {

    @Query("select user_action from UserAction user_action where user_action.createdBy.login = ?#{principal.username}")
    List<UserAction> findByCreatedByIsCurrentUser();
    
}
