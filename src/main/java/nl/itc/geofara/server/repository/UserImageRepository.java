package nl.itc.geofara.server.repository;

import nl.itc.geofara.server.domain.UserImage;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the UserImage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserImageRepository extends JpaRepository<UserImage, Long> {

    @Query("select user_image from UserImage user_image where user_image.createdBy.login = ?#{principal.username}")
    List<UserImage> findByCreatedByIsCurrentUser();

    @Query("select user_image from UserImage user_image where user_image.poi.id = :id")
    List<UserImage> findByPoi(@Param("id") Long id);
}
