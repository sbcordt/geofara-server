package nl.itc.geofara.server.repository;

import nl.itc.geofara.server.domain.UserText;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the UserText entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserTextRepository extends JpaRepository<UserText,Long> {

    @Query("select user_text from UserText user_text where user_text.createdBy.login = ?#{principal.username}")
    List<UserText> findByCreatedByIsCurrentUser();
    
}
