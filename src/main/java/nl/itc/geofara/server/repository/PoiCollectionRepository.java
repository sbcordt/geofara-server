package nl.itc.geofara.server.repository;

import nl.itc.geofara.server.domain.PoiCollection;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the PoiCollection entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PoiCollectionRepository extends JpaRepository<PoiCollection,Long> {

    @Query("select poi_collection from PoiCollection poi_collection where poi_collection.createdBy.login = ?#{principal.username}")
    List<PoiCollection> findByCreatedByIsCurrentUser();
    
    @Query("select distinct poi_collection from PoiCollection poi_collection left join fetch poi_collection.owningUserGroups")
    List<PoiCollection> findAllWithEagerRelationships();

    @Query("select poi_collection from PoiCollection poi_collection left join fetch poi_collection.owningUserGroups where poi_collection.id =:id")
    PoiCollection findOneWithEagerRelationships(@Param("id") Long id);
    
}
