package nl.itc.geofara.server.service;

import nl.itc.geofara.server.domain.User;
import nl.itc.geofara.server.domain.UserGroup;
import nl.itc.geofara.server.repository.UserGroupRepository;
import nl.itc.geofara.server.service.dto.UserGroupDTO;
import nl.itc.geofara.server.service.mapper.UserGroupMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Service Implementation for managing UserGroup.
 */
@Service
@Transactional
public class UserGroupService {

    private final Logger log = LoggerFactory.getLogger(UserGroupService.class);

    private final UserGroupRepository userGroupRepository;

    private final UserGroupMapper userGroupMapper;

    private final UserService userService;

    public UserGroupService(UserGroupRepository userGroupRepository, UserGroupMapper userGroupMapper, UserService userService) {
        this.userGroupRepository = userGroupRepository;
        this.userGroupMapper = userGroupMapper;
        this.userService = userService;
    }

    /**
     * Save a userGroup.
     *
     * @param userGroupDTO the entity to save
     * @return the persisted entity
     */
    public UserGroupDTO save(UserGroupDTO userGroupDTO) {
        log.debug("Request to save UserGroup : {}", userGroupDTO);
        UserGroup userGroup = userGroupMapper.toEntity(userGroupDTO);
        userGroup = userGroupRepository.save(userGroup);
        return userGroupMapper.toDto(userGroup);
    }

    /**
     *  Get all the userGroups.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserGroupDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserGroups");
        return userGroupRepository.findAll(pageable)
            .map(userGroupMapper::toDto);
    }

    /**
     *  Get one userGroup by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public UserGroupDTO findOne(Long id) {
        log.debug("Request to get UserGroup : {}", id);
        UserGroup userGroup = userGroupRepository.findOneWithEagerRelationships(id);
        return userGroupMapper.toDto(userGroup);
    }

    /**
     *  Delete the  userGroup by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete UserGroup : {}", id);
        userGroupRepository.delete(id);
    }

    public List<UserGroupDTO> findAllForCurrentUser() {
        User currentUser = userService.getUserWithAuthorities();
        return userGroupRepository
            .findAll()
            .stream()
            .filter(userGroup -> userGroup.getUsers().contains(currentUser))
            .map(userGroupMapper::toDto)
            .collect(Collectors.toList());
    }
}
