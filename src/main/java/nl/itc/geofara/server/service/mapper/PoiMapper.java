package nl.itc.geofara.server.service.mapper;

import nl.itc.geofara.server.domain.*;
import nl.itc.geofara.server.service.dto.PoiDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Poi and its DTO PoiDTO.
 */
@Mapper(componentModel = "spring", uses = {PoiCollectionMapper.class, })
public interface PoiMapper extends EntityMapper <PoiDTO, Poi> {

    @Mapping(source = "poiCollection.id", target = "poiCollectionId")
    @Mapping(source = "poiCollection.name", target = "poiCollectionName")
    PoiDTO toDto(Poi poi); 

    @Mapping(source = "poiCollectionId", target = "poiCollection")
    Poi toEntity(PoiDTO poiDTO); 
    default Poi fromId(Long id) {
        if (id == null) {
            return null;
        }
        Poi poi = new Poi();
        poi.setId(id);
        return poi;
    }
}
