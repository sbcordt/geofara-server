package nl.itc.geofara.server.service;

import nl.itc.geofara.server.domain.PoiCollection;
import nl.itc.geofara.server.domain.User;
import nl.itc.geofara.server.domain.UserGroup;
import nl.itc.geofara.server.repository.PoiCollectionRepository;
import nl.itc.geofara.server.repository.UserGroupRepository;
import nl.itc.geofara.server.repository.UserRepository;
import nl.itc.geofara.server.security.SecurityUtils;
import nl.itc.geofara.server.service.dto.PoiCollectionDTO;
import nl.itc.geofara.server.service.mapper.PoiCollectionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Service Implementation for managing PoiCollection.
 */
@Service
@Transactional
public class PoiCollectionService {

    private final Logger log = LoggerFactory.getLogger(PoiCollectionService.class);

    private final PoiCollectionRepository poiCollectionRepository;

    private final PoiCollectionMapper poiCollectionMapper;

    private final UserRepository userRepository;
    private UserGroupRepository userGroupRepository;

    public PoiCollectionService(PoiCollectionRepository poiCollectionRepository, PoiCollectionMapper poiCollectionMapper, UserRepository userRepository, UserGroupRepository userGroupRepository) {
        this.poiCollectionRepository = poiCollectionRepository;
        this.poiCollectionMapper = poiCollectionMapper;
        this.userRepository = userRepository;
        this.userGroupRepository = userGroupRepository;
    }

    /**
     * Save a poiCollection.
     *
     * @param poiCollectionDTO the entity to save
     * @return the persisted entity
     */
    public PoiCollectionDTO save(PoiCollectionDTO poiCollectionDTO) {
        log.debug("Request to save PoiCollection : {}", poiCollectionDTO);
        PoiCollection poiCollection = poiCollectionMapper.toEntity(poiCollectionDTO);
        poiCollection = poiCollectionRepository.save(poiCollection);
        return poiCollectionMapper.toDto(poiCollection);
    }

    /**
     *  Get all the poiCollections.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PoiCollectionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PoiCollections");
        return poiCollectionRepository.findAll(pageable)
            .map(poiCollectionMapper::toDto);
    }

    /**
     *  Get one poiCollection by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PoiCollectionDTO findOne(Long id) {
        log.debug("Request to get PoiCollection : {}", id);
        PoiCollection poiCollection = poiCollectionRepository.findOneWithEagerRelationships(id);
        return poiCollectionMapper.toDto(poiCollection);
    }

    /**
     *  Delete the  poiCollection by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PoiCollection : {}", id);
        poiCollectionRepository.delete(id);
    }


    public Set<PoiCollectionDTO> findByUserId(Long id) {
        User user = userRepository.findOneWithAuthoritiesById(id);
        List<UserGroup> userGroupsOfUser = userGroupRepository.findAllWithEagerRelationships();
        userGroupsOfUser.removeIf(userGroup -> !userGroup.getUsers().contains(user));

        return userGroupsOfUser
            .stream()
            .flatMap(userGroup -> userGroup.getOwningPoiCollections().stream())
            .map(poiCollectionMapper::toDto)
            .collect(Collectors.toSet());
    }

    public Set<PoiCollectionDTO> findByCurrentUser() {
        String currentUserLogin = SecurityUtils.getCurrentUserLogin();
        Optional<User> user = userRepository.findOneByLogin(currentUserLogin);
        User user1 = user.orElseThrow(NullPointerException::new);
        return findByUserId(user1.getId());
    }
}
