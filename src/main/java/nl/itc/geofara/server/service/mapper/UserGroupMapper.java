package nl.itc.geofara.server.service.mapper;

import nl.itc.geofara.server.domain.*;
import nl.itc.geofara.server.service.dto.UserGroupDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserGroup and its DTO UserGroupDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface UserGroupMapper extends EntityMapper <UserGroupDTO, UserGroup> {
    
    @Mapping(target = "owningPoiCollections", ignore = true)
    UserGroup toEntity(UserGroupDTO userGroupDTO); 
    default UserGroup fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserGroup userGroup = new UserGroup();
        userGroup.setId(id);
        return userGroup;
    }
}
