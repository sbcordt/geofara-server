package nl.itc.geofara.server.service;

import nl.itc.geofara.server.domain.User;
import nl.itc.geofara.server.domain.UserText;
import nl.itc.geofara.server.repository.UserTextRepository;
import nl.itc.geofara.server.service.dto.UserTextDTO;
import nl.itc.geofara.server.service.mapper.UserTextMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing UserText.
 */
@Service
@Transactional
public class UserTextService {

    private final Logger log = LoggerFactory.getLogger(UserTextService.class);

    private final UserTextRepository userTextRepository;

    private final UserTextMapper userTextMapper;

    private final UserService userService;

    public UserTextService(UserTextRepository userTextRepository, UserTextMapper userTextMapper, UserService userService) {
        this.userTextRepository = userTextRepository;
        this.userTextMapper = userTextMapper;
        this.userService = userService;
    }

    /**
     * Save a userText.
     *
     * @param userTextDTO the entity to save
     * @return the persisted entity
     */
    public UserTextDTO save(UserTextDTO userTextDTO) {
        log.debug("Request to save UserText : {}", userTextDTO);
        UserText userText = userTextMapper.toEntity(userTextDTO);
        userText = userTextRepository.save(userText);
        return userTextMapper.toDto(userText);
    }

    /**
     *  Get all the userTexts.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserTextDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserTexts");
        return userTextRepository.findAll(pageable)
            .map(userTextMapper::toDto);
    }

    /**
     *  Get one userText by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public UserTextDTO findOne(Long id) {
        log.debug("Request to get UserText : {}", id);
        UserText userText = userTextRepository.findOne(id);
        return userTextMapper.toDto(userText);
    }

    /**
     *  Delete the  userText by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete UserText : {}", id);
        userTextRepository.delete(id);
    }

    public UserTextDTO saveAsCurrentUser(UserTextDTO userTextDTO) {
        User user = userService.getUserWithAuthorities();
        userTextDTO.setCreatedById(user.getId());
        userTextDTO.setCreatedByLogin(user.getLogin());
        return save(userTextDTO);
    }
}
