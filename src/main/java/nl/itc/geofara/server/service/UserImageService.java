package nl.itc.geofara.server.service;

import nl.itc.geofara.server.domain.User;
import nl.itc.geofara.server.domain.UserImage;
import nl.itc.geofara.server.repository.UserImageRepository;
import nl.itc.geofara.server.service.dto.UserImageDTO;
import nl.itc.geofara.server.service.mapper.UserImageMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Service Implementation for managing UserImage.
 */
@Service
@Transactional
public class UserImageService {

    private final Logger log = LoggerFactory.getLogger(UserImageService.class);
    private final UserImageRepository userImageRepository;
    private final UserImageMapper userImageMapper;
    private final UserService userService;

    public UserImageService(UserImageRepository userImageRepository, UserImageMapper userImageMapper, UserService userService) {
        this.userImageRepository = userImageRepository;
        this.userImageMapper = userImageMapper;
        this.userService = userService;
    }

    /**
     * Save a userImage.
     *
     * @param userImageDTO the entity to save
     * @return the persisted entity
     */
    public UserImageDTO save(UserImageDTO userImageDTO) {
        log.debug("Request to save UserImage : {}", userImageDTO);
        UserImage userImage = userImageMapper.toEntity(userImageDTO);
        userImage = userImageRepository.save(userImage);
        return userImageMapper.toDto(userImage);
    }

    /**
     *  Get all the userImages.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserImageDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserImages");
        return userImageRepository.findAll(pageable)
            .map(userImageMapper::toDto);
    }

    /**
     *  Get one userImage by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public UserImageDTO findOne(Long id) {
        log.debug("Request to get UserImage : {}", id);
        UserImage userImage = userImageRepository.findOne(id);
        return userImageMapper.toDto(userImage);
    }

    /**
     *  Delete the  userImage by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete UserImage : {}", id);
        userImageRepository.delete(id);
    }

    public UserImageDTO saveAsCurrentUser(UserImageDTO userImageDTO) {

        User user = userService.getUserWithAuthorities();
        userImageDTO.setCreatedById(user.getId());
        userImageDTO.setCreatedByLogin(user.getLogin());
        return save(userImageDTO);
    }

    public List<UserImageDTO> findByPoi(Long id) {
        return userImageRepository
            .findByPoi(id)
            .stream()
            .map(userImageMapper::toDto)
            .collect(Collectors.toList());
    }
}
