package nl.itc.geofara.server.service.mapper;

import nl.itc.geofara.server.domain.*;
import nl.itc.geofara.server.service.dto.PoiCollectionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity PoiCollection and its DTO PoiCollectionDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, UserGroupMapper.class, })
public interface PoiCollectionMapper extends EntityMapper <PoiCollectionDTO, PoiCollection> {

    @Mapping(source = "createdBy.id", target = "createdById")
    @Mapping(source = "createdBy.login", target = "createdByLogin")
    PoiCollectionDTO toDto(PoiCollection poiCollection); 

    @Mapping(source = "createdById", target = "createdBy")
    PoiCollection toEntity(PoiCollectionDTO poiCollectionDTO); 
    default PoiCollection fromId(Long id) {
        if (id == null) {
            return null;
        }
        PoiCollection poiCollection = new PoiCollection();
        poiCollection.setId(id);
        return poiCollection;
    }
}
