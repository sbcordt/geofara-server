package nl.itc.geofara.server.service.mapper;

import nl.itc.geofara.server.domain.*;
import nl.itc.geofara.server.service.dto.UserActivityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserActivity and its DTO UserActivityDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface UserActivityMapper extends EntityMapper <UserActivityDTO, UserActivity> {

    @Mapping(source = "createdBy.id", target = "createdById")
    @Mapping(source = "createdBy.login", target = "createdByLogin")
    UserActivityDTO toDto(UserActivity userActivity); 

    @Mapping(source = "createdById", target = "createdBy")
    UserActivity toEntity(UserActivityDTO userActivityDTO); 
    default UserActivity fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserActivity userActivity = new UserActivity();
        userActivity.setId(id);
        return userActivity;
    }
}
