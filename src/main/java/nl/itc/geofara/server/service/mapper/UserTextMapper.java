package nl.itc.geofara.server.service.mapper;

import nl.itc.geofara.server.domain.*;
import nl.itc.geofara.server.service.dto.UserTextDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserText and its DTO UserTextDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface UserTextMapper extends EntityMapper <UserTextDTO, UserText> {

    @Mapping(source = "createdBy.id", target = "createdById")
    @Mapping(source = "createdBy.login", target = "createdByLogin")
    UserTextDTO toDto(UserText userText); 

    @Mapping(source = "createdById", target = "createdBy")
    UserText toEntity(UserTextDTO userTextDTO); 
    default UserText fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserText userText = new UserText();
        userText.setId(id);
        return userText;
    }
}
