package nl.itc.geofara.server.service;

import nl.itc.geofara.server.domain.UserActivity;
import nl.itc.geofara.server.repository.UserActivityRepository;
import nl.itc.geofara.server.service.dto.UserActivityDTO;
import nl.itc.geofara.server.service.mapper.UserActivityMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing UserActivity.
 */
@Service
@Transactional
public class UserActivityService {

    private final Logger log = LoggerFactory.getLogger(UserActivityService.class);

    private final UserActivityRepository userActivityRepository;

    private final UserActivityMapper userActivityMapper;
    private final UserService userService;

    public UserActivityService(UserActivityRepository userActivityRepository, UserActivityMapper userActivityMapper, UserService userService) {
        this.userActivityRepository = userActivityRepository;
        this.userActivityMapper = userActivityMapper;
        this.userService = userService;
    }

    /**
     * Save a userActivity.
     *
     * @param userActivityDTO the entity to save
     * @return the persisted entity
     */
    public UserActivityDTO save(UserActivityDTO userActivityDTO) {
        log.debug("Request to save UserActivity : {}", userActivityDTO);
        UserActivity userActivity = userActivityMapper.toEntity(userActivityDTO);
        userActivity.setCreatedBy(userService.getUserWithAuthorities());
        userActivity = userActivityRepository.save(userActivity);
        return userActivityMapper.toDto(userActivity);
    }

    /**
     *  Get all the userActivities.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserActivityDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserActivities");
        return userActivityRepository.findAll(pageable)
            .map(userActivityMapper::toDto);
    }

    /**
     *  Get one userActivity by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public UserActivityDTO findOne(Long id) {
        log.debug("Request to get UserActivity : {}", id);
        UserActivity userActivity = userActivityRepository.findOne(id);
        return userActivityMapper.toDto(userActivity);
    }

    /**
     *  Delete the  userActivity by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete UserActivity : {}", id);
        userActivityRepository.delete(id);
    }
}
