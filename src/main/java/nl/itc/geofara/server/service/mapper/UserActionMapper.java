package nl.itc.geofara.server.service.mapper;

import nl.itc.geofara.server.domain.*;
import nl.itc.geofara.server.service.dto.UserActionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserAction and its DTO UserActionDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, })
public interface UserActionMapper extends EntityMapper <UserActionDTO, UserAction> {

    @Mapping(source = "createdBy.id", target = "createdById")
    @Mapping(source = "createdBy.login", target = "createdByLogin")
    UserActionDTO toDto(UserAction userAction); 

    @Mapping(source = "createdById", target = "createdBy")
    UserAction toEntity(UserActionDTO userActionDTO); 
    default UserAction fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserAction userAction = new UserAction();
        userAction.setId(id);
        return userAction;
    }
}
