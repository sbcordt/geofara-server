package nl.itc.geofara.server.service;

import nl.itc.geofara.server.domain.Poi;
import nl.itc.geofara.server.repository.PoiRepository;
import nl.itc.geofara.server.service.dto.PoiDTO;
import nl.itc.geofara.server.service.mapper.PoiMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Service Implementation for managing Poi.
 */
@Service
@Transactional
public class PoiService {

    private final Logger log = LoggerFactory.getLogger(PoiService.class);

    private final PoiRepository poiRepository;

    private final PoiMapper poiMapper;

    public PoiService(PoiRepository poiRepository, PoiMapper poiMapper) {
        this.poiRepository = poiRepository;
        this.poiMapper = poiMapper;
    }

    /**
     * Save a poi.
     *
     * @param poiDTO the entity to save
     * @return the persisted entity
     */
    public PoiDTO save(PoiDTO poiDTO) {
        log.debug("Request to save Poi : {}", poiDTO);
        Poi poi = poiMapper.toEntity(poiDTO);
        poi = poiRepository.save(poi);
        return poiMapper.toDto(poi);
    }

    /**
     *  Get all the pois.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<PoiDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Pois");
        return poiRepository.findAll(pageable)
            .map(poiMapper::toDto);
    }

    /**
     *  Get one poi by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public PoiDTO findOne(Long id) {
        log.debug("Request to get Poi : {}", id);
        Poi poi = poiRepository.findOne(id);
        return poiMapper.toDto(poi);
    }

    /**
     *  Delete the  poi by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Poi : {}", id);
        poiRepository.delete(id);
    }

    public List<PoiDTO> findByPoiCollectionId(Long id) {
        log.debug("Request to get List<Poi> by PoiCollection : {}", id);
        List<Poi> pois = poiRepository.findByCollectionId(id);
        List<PoiDTO> poiDTOs = poiMapper.toDto(pois);
        return poiDTOs;
    }
}
