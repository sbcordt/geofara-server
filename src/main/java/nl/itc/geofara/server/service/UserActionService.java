package nl.itc.geofara.server.service;

import nl.itc.geofara.server.domain.UserAction;
import nl.itc.geofara.server.repository.UserActionRepository;
import nl.itc.geofara.server.service.dto.UserActionDTO;
import nl.itc.geofara.server.service.mapper.UserActionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing UserAction.
 */
@Service
@Transactional
public class UserActionService {

    private final Logger log = LoggerFactory.getLogger(UserActionService.class);

    private final UserActionRepository userActionRepository;

    private final UserActionMapper userActionMapper;

    public UserActionService(UserActionRepository userActionRepository, UserActionMapper userActionMapper) {
        this.userActionRepository = userActionRepository;
        this.userActionMapper = userActionMapper;
    }

    /**
     * Save a userAction.
     *
     * @param userActionDTO the entity to save
     * @return the persisted entity
     */
    public UserActionDTO save(UserActionDTO userActionDTO) {
        log.debug("Request to save UserAction : {}", userActionDTO);
        UserAction userAction = userActionMapper.toEntity(userActionDTO);
        userAction = userActionRepository.save(userAction);
        return userActionMapper.toDto(userAction);
    }

    /**
     *  Get all the userActions.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<UserActionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserActions");
        return userActionRepository.findAll(pageable)
            .map(userActionMapper::toDto);
    }

    /**
     *  Get one userAction by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public UserActionDTO findOne(Long id) {
        log.debug("Request to get UserAction : {}", id);
        UserAction userAction = userActionRepository.findOne(id);
        return userActionMapper.toDto(userAction);
    }

    /**
     *  Delete the  userAction by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete UserAction : {}", id);
        userActionRepository.delete(id);
    }
}
