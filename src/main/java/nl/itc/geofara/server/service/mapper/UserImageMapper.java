package nl.itc.geofara.server.service.mapper;

import nl.itc.geofara.server.domain.*;
import nl.itc.geofara.server.service.dto.UserImageDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity UserImage and its DTO UserImageDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class, PoiMapper.class, PoiCollectionMapper.class, })
public interface UserImageMapper extends EntityMapper <UserImageDTO, UserImage> {

    @Mapping(source = "createdBy.id", target = "createdById")
    @Mapping(source = "createdBy.login", target = "createdByLogin")

    @Mapping(source = "poi.id", target = "poiId")

    @Mapping(source = "poiCollection.id", target = "poiCollectionId")
    UserImageDTO toDto(UserImage userImage); 

    @Mapping(source = "createdById", target = "createdBy")

    @Mapping(source = "poiId", target = "poi")

    @Mapping(source = "poiCollectionId", target = "poiCollection")
    UserImage toEntity(UserImageDTO userImageDTO); 
    default UserImage fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserImage userImage = new UserImage();
        userImage.setId(id);
        return userImage;
    }
}
