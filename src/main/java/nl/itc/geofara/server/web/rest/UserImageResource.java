package nl.itc.geofara.server.web.rest;

import com.codahale.metrics.annotation.Timed;
import nl.itc.geofara.server.service.UserImageService;
import nl.itc.geofara.server.web.rest.util.HeaderUtil;
import nl.itc.geofara.server.web.rest.util.PaginationUtil;
import nl.itc.geofara.server.service.dto.UserImageDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UserImage.
 */
@RestController
@RequestMapping("/api")
public class UserImageResource {

    private final Logger log = LoggerFactory.getLogger(UserImageResource.class);

    private static final String ENTITY_NAME = "userImage";

    private final UserImageService userImageService;

    public UserImageResource(UserImageService userImageService) {
        this.userImageService = userImageService;
    }

    /**
     * POST  /user-images : Create a new userImage.
     *
     * @param photo the userImageDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userImageDTO, or with status 400 (Bad Request) if the userImage has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-images")
    @Timed
    public ResponseEntity<UserImageDTO> createUserImage(@RequestParam("photo") MultipartFile photo,
                                                        @RequestParam("title") String title,
                                                        @RequestParam("description") String description,
                                                        @RequestParam("latitude") String latitude,
                                                        @RequestParam("longitude") String longitude,
                                                        @RequestParam("createdAt") String createdAt,
                                                        @RequestParam("updatedAt") String updatedAt) throws URISyntaxException, IOException {

        UserImageDTO userImageDTO = new UserImageDTO();
        userImageDTO.setTitle(title);
        userImageDTO.setDescription(description);
        userImageDTO.setLatitude(Double.valueOf(latitude));
        userImageDTO.setLongitude(Double.valueOf(longitude));
        userImageDTO.setImage(photo.getBytes());
        userImageDTO.setImageContentType("image/jpeg");
        userImageDTO.setCreatedAt(ZonedDateTime.parse(createdAt));
        userImageDTO.setUpdatedAt(ZonedDateTime.parse(updatedAt));

        UserImageDTO result = userImageService.saveAsCurrentUser(userImageDTO);

        return ResponseEntity.created(new URI("/api/user-images/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-images : Updates an existing userImage.
     *
     * @param userImageDTO the userImageDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userImageDTO,
     * or with status 400 (Bad Request) if the userImageDTO is not valid,
     * or with status 500 (Internal Server Error) if the userImageDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-images")
    @Timed
    public ResponseEntity<UserImageDTO> updateUserImage(@Valid @RequestBody UserImageDTO userImageDTO) throws URISyntaxException {
        log.debug("REST request to update UserImage : {}", userImageDTO);
        if (userImageDTO.getId() == null) {
            //return createUserImage(userImageDTO);
        }
        UserImageDTO result = userImageService.save(userImageDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userImageDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-images : get all the userImages.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of userImages in body
     */
    @GetMapping("/user-images")
    @Timed
    public ResponseEntity<List<UserImageDTO>> getAllUserImages(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UserImages");
        Page<UserImageDTO> page = userImageService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-images");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /user-images/:id : get the "id" userImage.
     *
     * @param id the id of the userImageDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userImageDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-images/{id}")
    @Timed
    public ResponseEntity<UserImageDTO> getUserImage(@PathVariable Long id) {
        log.debug("REST request to get UserImage : {}", id);
        UserImageDTO userImageDTO = userImageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(userImageDTO));
    }

    /**
     * GET  /user-images/:id : get userimageDTO for poi with "id"
     *
     * @param id the id of the userImageDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userImageDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-images/pois/{id}")
    @Timed
    public ResponseEntity<List<UserImageDTO>> getUserImagesForPoi(@PathVariable Long id) {
        log.debug("REST request to get UserImage : {}", id);
        List<UserImageDTO> userImageDTO = userImageService.findByPoi(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(userImageDTO));
    }

    /**
     * DELETE  /user-images/:id : delete the "id" userImage.
     *
     * @param id the id of the userImageDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-images/{id}")
    @Timed
    public ResponseEntity<Void> deleteUserImage(@PathVariable Long id) {
        log.debug("REST request to delete UserImage : {}", id);
        userImageService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
