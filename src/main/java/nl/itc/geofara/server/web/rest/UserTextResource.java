package nl.itc.geofara.server.web.rest;

import com.codahale.metrics.annotation.Timed;
import nl.itc.geofara.server.service.UserTextService;
import nl.itc.geofara.server.web.rest.util.HeaderUtil;
import nl.itc.geofara.server.web.rest.util.PaginationUtil;
import nl.itc.geofara.server.service.dto.UserTextDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UserText.
 */
@RestController
@RequestMapping("/api")
public class UserTextResource {

    private final Logger log = LoggerFactory.getLogger(UserTextResource.class);

    private static final String ENTITY_NAME = "userText";

    private final UserTextService userTextService;

    public UserTextResource(UserTextService userTextService) {
        this.userTextService = userTextService;
    }

    /**
     * POST  /user-texts : Create a new userText.
     *
     * @param userTextDTO the userTextDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userTextDTO, or with status 400 (Bad Request) if the userText has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-texts")
    @Timed
    public ResponseEntity<UserTextDTO> createUserText(@Valid @RequestBody UserTextDTO userTextDTO) throws URISyntaxException {
        log.debug("REST request to save UserText : {}", userTextDTO);
        if (userTextDTO.getId() != null) {
            UserTextDTO result = userTextService.save(userTextDTO);
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userTextDTO.getId().toString()))
                .body(result);
        }
        UserTextDTO result = userTextService.saveAsCurrentUser(userTextDTO);
        return ResponseEntity.created(new URI("/api/user-texts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-texts : Updates an existing userText.
     *
     * @param userTextDTO the userTextDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userTextDTO,
     * or with status 400 (Bad Request) if the userTextDTO is not valid,
     * or with status 500 (Internal Server Error) if the userTextDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-texts")
    @Timed
    public ResponseEntity<UserTextDTO> updateUserText(@Valid @RequestBody UserTextDTO userTextDTO) throws URISyntaxException {
        log.debug("REST request to update UserText : {}", userTextDTO);
        if (userTextDTO.getId() == null) {
            return createUserText(userTextDTO);
        }
        UserTextDTO result = userTextService.save(userTextDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userTextDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-texts : get all the userTexts.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of userTexts in body
     */
    @GetMapping("/user-texts")
    @Timed
    public ResponseEntity<List<UserTextDTO>> getAllUserTexts(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UserTexts");
        Page<UserTextDTO> page = userTextService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-texts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /user-texts/:id : get the "id" userText.
     *
     * @param id the id of the userTextDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userTextDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-texts/{id}")
    @Timed
    public ResponseEntity<UserTextDTO> getUserText(@PathVariable Long id) {
        log.debug("REST request to get UserText : {}", id);
        UserTextDTO userTextDTO = userTextService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(userTextDTO));
    }

    /**
     * DELETE  /user-texts/:id : delete the "id" userText.
     *
     * @param id the id of the userTextDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-texts/{id}")
    @Timed
    public ResponseEntity<Void> deleteUserText(@PathVariable Long id) {
        log.debug("REST request to delete UserText : {}", id);
        userTextService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
