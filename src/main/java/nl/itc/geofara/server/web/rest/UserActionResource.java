package nl.itc.geofara.server.web.rest;

import com.codahale.metrics.annotation.Timed;
import nl.itc.geofara.server.service.UserActionService;
import nl.itc.geofara.server.web.rest.util.HeaderUtil;
import nl.itc.geofara.server.web.rest.util.PaginationUtil;
import nl.itc.geofara.server.service.dto.UserActionDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UserAction.
 */
@RestController
@RequestMapping("/api")
public class UserActionResource {

    private final Logger log = LoggerFactory.getLogger(UserActionResource.class);

    private static final String ENTITY_NAME = "userAction";

    private final UserActionService userActionService;

    public UserActionResource(UserActionService userActionService) {
        this.userActionService = userActionService;
    }

    /**
     * POST  /user-actions : Create a new userAction.
     *
     * @param userActionDTO the userActionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userActionDTO, or with status 400 (Bad Request) if the userAction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-actions")
    @Timed
    public ResponseEntity<UserActionDTO> createUserAction(@Valid @RequestBody UserActionDTO userActionDTO) throws URISyntaxException {
        log.debug("REST request to save UserAction : {}", userActionDTO);
        if (userActionDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new userAction cannot already have an ID")).body(null);
        }
        UserActionDTO result = userActionService.save(userActionDTO);
        return ResponseEntity.created(new URI("/api/user-actions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-actions : Updates an existing userAction.
     *
     * @param userActionDTO the userActionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userActionDTO,
     * or with status 400 (Bad Request) if the userActionDTO is not valid,
     * or with status 500 (Internal Server Error) if the userActionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-actions")
    @Timed
    public ResponseEntity<UserActionDTO> updateUserAction(@Valid @RequestBody UserActionDTO userActionDTO) throws URISyntaxException {
        log.debug("REST request to update UserAction : {}", userActionDTO);
        if (userActionDTO.getId() == null) {
            return createUserAction(userActionDTO);
        }
        UserActionDTO result = userActionService.save(userActionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userActionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-actions : get all the userActions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of userActions in body
     */
    @GetMapping("/user-actions")
    @Timed
    public ResponseEntity<List<UserActionDTO>> getAllUserActions(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UserActions");
        Page<UserActionDTO> page = userActionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-actions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /user-actions/:id : get the "id" userAction.
     *
     * @param id the id of the userActionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userActionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-actions/{id}")
    @Timed
    public ResponseEntity<UserActionDTO> getUserAction(@PathVariable Long id) {
        log.debug("REST request to get UserAction : {}", id);
        UserActionDTO userActionDTO = userActionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(userActionDTO));
    }

    /**
     * DELETE  /user-actions/:id : delete the "id" userAction.
     *
     * @param id the id of the userActionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-actions/{id}")
    @Timed
    public ResponseEntity<Void> deleteUserAction(@PathVariable Long id) {
        log.debug("REST request to delete UserAction : {}", id);
        userActionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
