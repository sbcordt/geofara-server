package nl.itc.geofara.server.web.rest;

import com.codahale.metrics.annotation.Timed;
import nl.itc.geofara.server.service.PoiCollectionService;
import nl.itc.geofara.server.web.rest.util.HeaderUtil;
import nl.itc.geofara.server.web.rest.util.PaginationUtil;
import nl.itc.geofara.server.service.dto.PoiCollectionDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing PoiCollection.
 */
@RestController
@RequestMapping("/api")
public class PoiCollectionResource {

    private final Logger log = LoggerFactory.getLogger(PoiCollectionResource.class);

    private static final String ENTITY_NAME = "poiCollection";

    private final PoiCollectionService poiCollectionService;

    public PoiCollectionResource(PoiCollectionService poiCollectionService) {
        this.poiCollectionService = poiCollectionService;
    }

    /**
     * POST  /poi-collections : Create a new poiCollection.
     *
     * @param poiCollectionDTO the poiCollectionDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new poiCollectionDTO, or with status 400 (Bad Request) if the poiCollection has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/poi-collections")
    @Timed
    public ResponseEntity<PoiCollectionDTO> createPoiCollection(@Valid @RequestBody PoiCollectionDTO poiCollectionDTO) throws URISyntaxException {
        log.debug("REST request to save PoiCollection : {}", poiCollectionDTO);
        if (poiCollectionDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new poiCollection cannot already have an ID")).body(null);
        }
        PoiCollectionDTO result = poiCollectionService.save(poiCollectionDTO);
        return ResponseEntity.created(new URI("/api/poi-collections/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /poi-collections : Updates an existing poiCollection.
     *
     * @param poiCollectionDTO the poiCollectionDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated poiCollectionDTO,
     * or with status 400 (Bad Request) if the poiCollectionDTO is not valid,
     * or with status 500 (Internal Server Error) if the poiCollectionDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/poi-collections")
    @Timed
    public ResponseEntity<PoiCollectionDTO> updatePoiCollection(@Valid @RequestBody PoiCollectionDTO poiCollectionDTO) throws URISyntaxException {
        log.debug("REST request to update PoiCollection : {}", poiCollectionDTO);
        if (poiCollectionDTO.getId() == null) {
            return createPoiCollection(poiCollectionDTO);
        }
        PoiCollectionDTO result = poiCollectionService.save(poiCollectionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, poiCollectionDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /poi-collections : get all the poiCollections.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of poiCollections in body
     */
    @GetMapping("/poi-collections")
    @Timed
    public ResponseEntity<List<PoiCollectionDTO>> getAllPoiCollections(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PoiCollections");
        Page<PoiCollectionDTO> page = poiCollectionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/poi-collections");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /poi-collections/:id : get the "id" poiCollection.
     *
     * @param id the id of the poiCollectionDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the poiCollectionDTO, or with status 404 (Not Found)
     */
    @GetMapping("/poi-collections/{id}")
    @Timed
    public ResponseEntity<PoiCollectionDTO> getPoiCollection(@PathVariable Long id) {
        log.debug("REST request to get PoiCollection : {}", id);
        PoiCollectionDTO poiCollectionDTO = poiCollectionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(poiCollectionDTO));
    }

    /**
     * GET  /poi-collections/users/:id : get the "id" poiCollection.
     *
     * @param id the id of the user whose PoiCollections to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the Set<PoiCollectionDTO>, or with status 404 (Not Found)
     */
    @GetMapping("/poi-collections/users/{id}")
    @Timed
    public ResponseEntity<Set<PoiCollectionDTO>> getPoiCollectionsByUserId(@PathVariable Long id) {
        log.debug("REST request to get PoiCollection : {}", id);
        Set<PoiCollectionDTO> poiCollectionDTOS = poiCollectionService.findByUserId(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(poiCollectionDTOS));
    }

    /**
     * GET  /poi-collections/users/current-user : get all poiCollections for current user
     *
     * @return the ResponseEntity with status 200 (OK) and with body the Set<PoiCollectionDTO>, or with status 404 (Not Found)
     */
    @GetMapping("/poi-collections/users/current-user")
    @Timed
    public ResponseEntity<Set<PoiCollectionDTO>> getPoiCollectionsForCurrentUser() {
        log.debug("REST request to get PoiCollections for current user");
        Set<PoiCollectionDTO> poiCollectionDTOS = poiCollectionService.findByCurrentUser();
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(poiCollectionDTOS));
    }

    /**
     * DELETE  /poi-collections/:id : delete the "id" poiCollection.
     *
     * @param id the id of the poiCollectionDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/poi-collections/{id}")
    @Timed
    public ResponseEntity<Void> deletePoiCollection(@PathVariable Long id) {
        log.debug("REST request to delete PoiCollection : {}", id);
        poiCollectionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
