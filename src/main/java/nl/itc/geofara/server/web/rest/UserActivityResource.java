package nl.itc.geofara.server.web.rest;

import com.codahale.metrics.annotation.Timed;
import nl.itc.geofara.server.service.UserActivityService;
import nl.itc.geofara.server.web.rest.util.HeaderUtil;
import nl.itc.geofara.server.web.rest.util.PaginationUtil;
import nl.itc.geofara.server.service.dto.UserActivityDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing UserActivity.
 */
@RestController
@RequestMapping("/api")
public class UserActivityResource {

    private final Logger log = LoggerFactory.getLogger(UserActivityResource.class);

    private static final String ENTITY_NAME = "userActivity";

    private final UserActivityService userActivityService;

    public UserActivityResource(UserActivityService userActivityService) {
        this.userActivityService = userActivityService;
    }

    /**
     * POST  /user-activities : Create a new userActivity.
     *
     * @param userActivityDTO the userActivityDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userActivityDTO, or with status 400 (Bad Request) if the userActivity has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-activities")
    @Timed
    public ResponseEntity<UserActivityDTO> createUserActivity(@Valid @RequestBody UserActivityDTO userActivityDTO) throws URISyntaxException {
        log.debug("REST request to save UserActivity : {}", userActivityDTO);
        if (userActivityDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new userActivity cannot already have an ID")).body(null);
        }
        UserActivityDTO result = userActivityService.save(userActivityDTO);
        return ResponseEntity.created(new URI("/api/user-activities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-activities : Updates an existing userActivity.
     *
     * @param userActivityDTO the userActivityDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userActivityDTO,
     * or with status 400 (Bad Request) if the userActivityDTO is not valid,
     * or with status 500 (Internal Server Error) if the userActivityDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-activities")
    @Timed
    public ResponseEntity<UserActivityDTO> updateUserActivity(@Valid @RequestBody UserActivityDTO userActivityDTO) throws URISyntaxException {
        log.debug("REST request to update UserActivity : {}", userActivityDTO);
        if (userActivityDTO.getId() == null) {
            return createUserActivity(userActivityDTO);
        }
        UserActivityDTO result = userActivityService.save(userActivityDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userActivityDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-activities : get all the userActivities.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of userActivities in body
     */
    @GetMapping("/user-activities")
    @Timed
    public ResponseEntity<List<UserActivityDTO>> getAllUserActivities(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of UserActivities");
        Page<UserActivityDTO> page = userActivityService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/user-activities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /user-activities/:id : get the "id" userActivity.
     *
     * @param id the id of the userActivityDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userActivityDTO, or with status 404 (Not Found)
     */
    @GetMapping("/user-activities/{id}")
    @Timed
    public ResponseEntity<UserActivityDTO> getUserActivity(@PathVariable Long id) {
        log.debug("REST request to get UserActivity : {}", id);
        UserActivityDTO userActivityDTO = userActivityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(userActivityDTO));
    }

    /**
     * DELETE  /user-activities/:id : delete the "id" userActivity.
     *
     * @param id the id of the userActivityDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-activities/{id}")
    @Timed
    public ResponseEntity<Void> deleteUserActivity(@PathVariable Long id) {
        log.debug("REST request to delete UserActivity : {}", id);
        userActivityService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
