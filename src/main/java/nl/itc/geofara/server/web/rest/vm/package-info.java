/**
 * View Models used by Spring MVC REST controllers.
 */
package nl.itc.geofara.server.web.rest.vm;
