package nl.itc.geofara.server.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A UserAction.
 */
@Entity
@Table(name = "user_action")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserAction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "tag")
    private String tag;

    @Column(name = "view_group")
    private String viewGroup;

    @Column(name = "root_view")
    private String rootView;

    @Column(name = "action")
    private String action;

    @Column(name = "input_x")
    private Double inputX;

    @Column(name = "input_y")
    private Double inputY;

    @NotNull
    @Column(name = "latitude", nullable = false)
    private Double latitude;

    @NotNull
    @Column(name = "longitude", nullable = false)
    private Double longitude;

    @NotNull
    @Column(name = "created_at", nullable = false)
    private ZonedDateTime createdAt;

    @NotNull
    @Column(name = "updated_at", nullable = false)
    private ZonedDateTime updatedAt;

    @Column(name = "deleted_at")
    private ZonedDateTime deletedAt;

    @ManyToOne
    private User createdBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public UserAction tag(String tag) {
        this.tag = tag;
        return this;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getViewGroup() {
        return viewGroup;
    }

    public UserAction viewGroup(String viewGroup) {
        this.viewGroup = viewGroup;
        return this;
    }

    public void setViewGroup(String viewGroup) {
        this.viewGroup = viewGroup;
    }

    public String getRootView() {
        return rootView;
    }

    public UserAction rootView(String rootView) {
        this.rootView = rootView;
        return this;
    }

    public void setRootView(String rootView) {
        this.rootView = rootView;
    }

    public String getAction() {
        return action;
    }

    public UserAction action(String action) {
        this.action = action;
        return this;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Double getInputX() {
        return inputX;
    }

    public UserAction inputX(Double inputX) {
        this.inputX = inputX;
        return this;
    }

    public void setInputX(Double inputX) {
        this.inputX = inputX;
    }

    public Double getInputY() {
        return inputY;
    }

    public UserAction inputY(Double inputY) {
        this.inputY = inputY;
        return this;
    }

    public void setInputY(Double inputY) {
        this.inputY = inputY;
    }

    public Double getLatitude() {
        return latitude;
    }

    public UserAction latitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public UserAction longitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public UserAction createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public UserAction updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public ZonedDateTime getDeletedAt() {
        return deletedAt;
    }

    public UserAction deletedAt(ZonedDateTime deletedAt) {
        this.deletedAt = deletedAt;
        return this;
    }

    public void setDeletedAt(ZonedDateTime deletedAt) {
        this.deletedAt = deletedAt;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public UserAction createdBy(User user) {
        this.createdBy = user;
        return this;
    }

    public void setCreatedBy(User user) {
        this.createdBy = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserAction userAction = (UserAction) o;
        if (userAction.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userAction.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserAction{" +
            "id=" + getId() +
            ", tag='" + getTag() + "'" +
            ", viewGroup='" + getViewGroup() + "'" +
            ", rootView='" + getRootView() + "'" +
            ", action='" + getAction() + "'" +
            ", inputX='" + getInputX() + "'" +
            ", inputY='" + getInputY() + "'" +
            ", latitude='" + getLatitude() + "'" +
            ", longitude='" + getLongitude() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", deletedAt='" + getDeletedAt() + "'" +
            "}";
    }
}
