package nl.itc.geofara.server.domain.enumeration;

/**
 * The ImageType enumeration.
 */
public enum ImageType {
    SCREENSHOT, PHOTO
}
