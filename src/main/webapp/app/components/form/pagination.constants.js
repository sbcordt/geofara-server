(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
