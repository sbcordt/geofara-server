(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('UserGroupDetailController', UserGroupDetailController);

    UserGroupDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserGroup', 'User', 'PoiCollection'];

    function UserGroupDetailController($scope, $rootScope, $stateParams, previousState, entity, UserGroup, User, PoiCollection) {
        var vm = this;

        vm.userGroup = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('geofaraApp:userGroupUpdate', function(event, result) {
            vm.userGroup = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
