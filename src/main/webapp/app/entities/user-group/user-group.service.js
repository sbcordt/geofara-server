(function() {
    'use strict';
    angular
        .module('geofaraApp')
        .factory('UserGroup', UserGroup);

    UserGroup.$inject = ['$resource', 'DateUtils'];

    function UserGroup ($resource, DateUtils) {
        var resourceUrl =  'api/user-groups/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createdAt = DateUtils.convertDateTimeFromServer(data.createdAt);
                        data.updatedAt = DateUtils.convertDateTimeFromServer(data.updatedAt);
                        data.deletedAt = DateUtils.convertDateTimeFromServer(data.deletedAt);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
