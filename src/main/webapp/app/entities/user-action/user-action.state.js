(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-action', {
            parent: 'entity',
            url: '/user-action',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'geofaraApp.userAction.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-action/user-actions.html',
                    controller: 'UserActionController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userAction');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('user-action-detail', {
            parent: 'user-action',
            url: '/user-action/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'geofaraApp.userAction.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-action/user-action-detail.html',
                    controller: 'UserActionDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userAction');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'UserAction', function($stateParams, UserAction) {
                    return UserAction.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-action',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-action-detail.edit', {
            parent: 'user-action-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-action/user-action-dialog.html',
                    controller: 'UserActionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserAction', function(UserAction) {
                            return UserAction.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-action.new', {
            parent: 'user-action',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-action/user-action-dialog.html',
                    controller: 'UserActionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                tag: null,
                                viewGroup: null,
                                rootView: null,
                                action: null,
                                inputX: null,
                                inputY: null,
                                latitude: null,
                                longitude: null,
                                createdAt: null,
                                updatedAt: null,
                                deletedAt: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-action', null, { reload: 'user-action' });
                }, function() {
                    $state.go('user-action');
                });
            }]
        })
        .state('user-action.edit', {
            parent: 'user-action',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-action/user-action-dialog.html',
                    controller: 'UserActionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserAction', function(UserAction) {
                            return UserAction.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-action', null, { reload: 'user-action' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-action.delete', {
            parent: 'user-action',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-action/user-action-delete-dialog.html',
                    controller: 'UserActionDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserAction', function(UserAction) {
                            return UserAction.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-action', null, { reload: 'user-action' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
