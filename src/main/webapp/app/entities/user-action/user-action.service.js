(function() {
    'use strict';
    angular
        .module('geofaraApp')
        .factory('UserAction', UserAction);

    UserAction.$inject = ['$resource', 'DateUtils'];

    function UserAction ($resource, DateUtils) {
        var resourceUrl =  'api/user-actions/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createdAt = DateUtils.convertDateTimeFromServer(data.createdAt);
                        data.updatedAt = DateUtils.convertDateTimeFromServer(data.updatedAt);
                        data.deletedAt = DateUtils.convertDateTimeFromServer(data.deletedAt);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
