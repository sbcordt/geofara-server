(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('UserActionDeleteController',UserActionDeleteController);

    UserActionDeleteController.$inject = ['$uibModalInstance', 'entity', 'UserAction'];

    function UserActionDeleteController($uibModalInstance, entity, UserAction) {
        var vm = this;

        vm.userAction = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UserAction.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
