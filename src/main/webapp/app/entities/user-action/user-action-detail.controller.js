(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('UserActionDetailController', UserActionDetailController);

    UserActionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserAction', 'User'];

    function UserActionDetailController($scope, $rootScope, $stateParams, previousState, entity, UserAction, User) {
        var vm = this;

        vm.userAction = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('geofaraApp:userActionUpdate', function(event, result) {
            vm.userAction = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
