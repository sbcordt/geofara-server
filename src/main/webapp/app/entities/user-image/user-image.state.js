(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-image', {
            parent: 'entity',
            url: '/user-image',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'geofaraApp.userImage.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-image/user-images.html',
                    controller: 'UserImageController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userImage');
                    $translatePartialLoader.addPart('imageType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('user-image-detail', {
            parent: 'user-image',
            url: '/user-image/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'geofaraApp.userImage.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-image/user-image-detail.html',
                    controller: 'UserImageDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userImage');
                    $translatePartialLoader.addPart('imageType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'UserImage', function($stateParams, UserImage) {
                    return UserImage.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-image',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-image-detail.edit', {
            parent: 'user-image-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-image/user-image-dialog.html',
                    controller: 'UserImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserImage', function(UserImage) {
                            return UserImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-image.new', {
            parent: 'user-image',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-image/user-image-dialog.html',
                    controller: 'UserImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                title: null,
                                description: null,
                                imageType: null,
                                image: null,
                                imageContentType: null,
                                latitude: null,
                                longitude: null,
                                createdAt: null,
                                updatedAt: null,
                                deletedAt: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-image', null, { reload: 'user-image' });
                }, function() {
                    $state.go('user-image');
                });
            }]
        })
        .state('user-image.edit', {
            parent: 'user-image',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-image/user-image-dialog.html',
                    controller: 'UserImageDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserImage', function(UserImage) {
                            return UserImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-image', null, { reload: 'user-image' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-image.delete', {
            parent: 'user-image',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-image/user-image-delete-dialog.html',
                    controller: 'UserImageDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserImage', function(UserImage) {
                            return UserImage.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-image', null, { reload: 'user-image' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
