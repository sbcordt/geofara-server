(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('UserImageDetailController', UserImageDetailController);

    UserImageDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'UserImage', 'User', 'Poi', 'PoiCollection'];

    function UserImageDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, UserImage, User, Poi, PoiCollection) {
        var vm = this;

        vm.userImage = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('geofaraApp:userImageUpdate', function(event, result) {
            vm.userImage = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
