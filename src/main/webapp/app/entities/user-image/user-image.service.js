(function() {
    'use strict';
    angular
        .module('geofaraApp')
        .factory('UserImage', UserImage);

    UserImage.$inject = ['$resource', 'DateUtils'];

    function UserImage ($resource, DateUtils) {
        var resourceUrl =  'api/user-images/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createdAt = DateUtils.convertDateTimeFromServer(data.createdAt);
                        data.updatedAt = DateUtils.convertDateTimeFromServer(data.updatedAt);
                        data.deletedAt = DateUtils.convertDateTimeFromServer(data.deletedAt);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
