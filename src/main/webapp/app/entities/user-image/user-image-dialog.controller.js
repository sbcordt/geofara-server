(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('UserImageDialogController', UserImageDialogController);

    UserImageDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'UserImage', 'User', 'Poi', 'PoiCollection'];

    function UserImageDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, UserImage, User, Poi, PoiCollection) {
        var vm = this;

        vm.userImage = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;
        vm.users = User.query();
        vm.pois = Poi.query();
        vm.poicollections = PoiCollection.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userImage.id !== null) {
                UserImage.update(vm.userImage, onSaveSuccess, onSaveError);
            } else {
                UserImage.save(vm.userImage, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('geofaraApp:userImageUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


        vm.setImage = function ($file, userImage) {
            if ($file && $file.$error === 'pattern') {
                return;
            }
            if ($file) {
                DataUtils.toBase64($file, function(base64Data) {
                    $scope.$apply(function() {
                        userImage.image = base64Data;
                        userImage.imageContentType = $file.type;
                    });
                });
            }
        };
        vm.datePickerOpenStatus.createdAt = false;
        vm.datePickerOpenStatus.updatedAt = false;
        vm.datePickerOpenStatus.deletedAt = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
