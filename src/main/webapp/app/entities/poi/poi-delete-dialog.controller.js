(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('PoiDeleteController',PoiDeleteController);

    PoiDeleteController.$inject = ['$uibModalInstance', 'entity', 'Poi'];

    function PoiDeleteController($uibModalInstance, entity, Poi) {
        var vm = this;

        vm.poi = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Poi.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
