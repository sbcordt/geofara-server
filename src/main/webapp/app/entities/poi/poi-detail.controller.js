(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('PoiDetailController', PoiDetailController);

    PoiDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Poi', 'PoiCollection'];

    function PoiDetailController($scope, $rootScope, $stateParams, previousState, entity, Poi, PoiCollection) {
        var vm = this;

        vm.poi = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('geofaraApp:poiUpdate', function(event, result) {
            vm.poi = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
