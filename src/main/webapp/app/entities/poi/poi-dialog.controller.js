(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('PoiDialogController', PoiDialogController);

    PoiDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Poi', 'PoiCollection'];

    function PoiDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Poi, PoiCollection) {
        var vm = this;

        vm.poi = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.poicollections = PoiCollection.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.poi.id !== null) {
                Poi.update(vm.poi, onSaveSuccess, onSaveError);
            } else {
                Poi.save(vm.poi, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('geofaraApp:poiUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.createdAt = false;
        vm.datePickerOpenStatus.updatedAt = false;
        vm.datePickerOpenStatus.deletedAt = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
