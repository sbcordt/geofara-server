(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('poi', {
            parent: 'entity',
            url: '/poi',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'geofaraApp.poi.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/poi/pois.html',
                    controller: 'PoiController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('poi');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('poi-detail', {
            parent: 'poi',
            url: '/poi/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'geofaraApp.poi.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/poi/poi-detail.html',
                    controller: 'PoiDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('poi');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Poi', function($stateParams, Poi) {
                    return Poi.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'poi',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('poi-detail.edit', {
            parent: 'poi-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/poi/poi-dialog.html',
                    controller: 'PoiDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Poi', function(Poi) {
                            return Poi.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('poi.new', {
            parent: 'poi',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/poi/poi-dialog.html',
                    controller: 'PoiDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                description: null,
                                latitude: null,
                                longitude: null,
                                createdAt: null,
                                updatedAt: null,
                                deletedAt: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('poi', null, { reload: 'poi' });
                }, function() {
                    $state.go('poi');
                });
            }]
        })
        .state('poi.edit', {
            parent: 'poi',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/poi/poi-dialog.html',
                    controller: 'PoiDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Poi', function(Poi) {
                            return Poi.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('poi', null, { reload: 'poi' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('poi.delete', {
            parent: 'poi',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/poi/poi-delete-dialog.html',
                    controller: 'PoiDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Poi', function(Poi) {
                            return Poi.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('poi', null, { reload: 'poi' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
