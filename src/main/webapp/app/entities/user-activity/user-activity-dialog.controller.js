(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('UserActivityDialogController', UserActivityDialogController);

    UserActivityDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'UserActivity', 'User'];

    function UserActivityDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, UserActivity, User) {
        var vm = this;

        vm.userActivity = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userActivity.id !== null) {
                UserActivity.update(vm.userActivity, onSaveSuccess, onSaveError);
            } else {
                UserActivity.save(vm.userActivity, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('geofaraApp:userActivityUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.createdAt = false;
        vm.datePickerOpenStatus.updatedAt = false;
        vm.datePickerOpenStatus.deletedAt = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
