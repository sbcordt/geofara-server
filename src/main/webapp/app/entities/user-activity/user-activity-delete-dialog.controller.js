(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('UserActivityDeleteController',UserActivityDeleteController);

    UserActivityDeleteController.$inject = ['$uibModalInstance', 'entity', 'UserActivity'];

    function UserActivityDeleteController($uibModalInstance, entity, UserActivity) {
        var vm = this;

        vm.userActivity = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UserActivity.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
