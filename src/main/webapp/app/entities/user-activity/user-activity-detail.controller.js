(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('UserActivityDetailController', UserActivityDetailController);

    UserActivityDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserActivity', 'User'];

    function UserActivityDetailController($scope, $rootScope, $stateParams, previousState, entity, UserActivity, User) {
        var vm = this;

        vm.userActivity = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('geofaraApp:userActivityUpdate', function(event, result) {
            vm.userActivity = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
