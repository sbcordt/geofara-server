(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-activity', {
            parent: 'entity',
            url: '/user-activity',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'geofaraApp.userActivity.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-activity/user-activities.html',
                    controller: 'UserActivityController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userActivity');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('user-activity-detail', {
            parent: 'user-activity',
            url: '/user-activity/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'geofaraApp.userActivity.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-activity/user-activity-detail.html',
                    controller: 'UserActivityDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userActivity');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'UserActivity', function($stateParams, UserActivity) {
                    return UserActivity.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-activity',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-activity-detail.edit', {
            parent: 'user-activity-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-activity/user-activity-dialog.html',
                    controller: 'UserActivityDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserActivity', function(UserActivity) {
                            return UserActivity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-activity.new', {
            parent: 'user-activity',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-activity/user-activity-dialog.html',
                    controller: 'UserActivityDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                activity: null,
                                confidence: null,
                                latitude: null,
                                longitude: null,
                                accuracy: null,
                                createdAt: null,
                                updatedAt: null,
                                deletedAt: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-activity', null, { reload: 'user-activity' });
                }, function() {
                    $state.go('user-activity');
                });
            }]
        })
        .state('user-activity.edit', {
            parent: 'user-activity',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-activity/user-activity-dialog.html',
                    controller: 'UserActivityDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserActivity', function(UserActivity) {
                            return UserActivity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-activity', null, { reload: 'user-activity' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-activity.delete', {
            parent: 'user-activity',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-activity/user-activity-delete-dialog.html',
                    controller: 'UserActivityDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserActivity', function(UserActivity) {
                            return UserActivity.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-activity', null, { reload: 'user-activity' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
