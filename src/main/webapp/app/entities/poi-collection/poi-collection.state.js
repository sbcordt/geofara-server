(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('poi-collection', {
            parent: 'entity',
            url: '/poi-collection',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'geofaraApp.poiCollection.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/poi-collection/poi-collections.html',
                    controller: 'PoiCollectionController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('poiCollection');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('poi-collection-detail', {
            parent: 'poi-collection',
            url: '/poi-collection/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'geofaraApp.poiCollection.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/poi-collection/poi-collection-detail.html',
                    controller: 'PoiCollectionDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('poiCollection');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PoiCollection', function($stateParams, PoiCollection) {
                    return PoiCollection.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'poi-collection',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('poi-collection-detail.edit', {
            parent: 'poi-collection-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/poi-collection/poi-collection-dialog.html',
                    controller: 'PoiCollectionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PoiCollection', function(PoiCollection) {
                            return PoiCollection.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('poi-collection.new', {
            parent: 'poi-collection',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/poi-collection/poi-collection-dialog.html',
                    controller: 'PoiCollectionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                description: null,
                                createdAt: null,
                                updatedAt: null,
                                deletedAt: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('poi-collection', null, { reload: 'poi-collection' });
                }, function() {
                    $state.go('poi-collection');
                });
            }]
        })
        .state('poi-collection.edit', {
            parent: 'poi-collection',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/poi-collection/poi-collection-dialog.html',
                    controller: 'PoiCollectionDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PoiCollection', function(PoiCollection) {
                            return PoiCollection.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('poi-collection', null, { reload: 'poi-collection' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('poi-collection.delete', {
            parent: 'poi-collection',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/poi-collection/poi-collection-delete-dialog.html',
                    controller: 'PoiCollectionDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PoiCollection', function(PoiCollection) {
                            return PoiCollection.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('poi-collection', null, { reload: 'poi-collection' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
