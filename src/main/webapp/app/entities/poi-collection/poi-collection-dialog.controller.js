(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('PoiCollectionDialogController', PoiCollectionDialogController);

    PoiCollectionDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'PoiCollection', 'User', 'UserGroup'];

    function PoiCollectionDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, PoiCollection, User, UserGroup) {
        var vm = this;

        vm.poiCollection = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();
        vm.usergroups = UserGroup.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.poiCollection.id !== null) {
                PoiCollection.update(vm.poiCollection, onSaveSuccess, onSaveError);
            } else {
                PoiCollection.save(vm.poiCollection, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('geofaraApp:poiCollectionUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.createdAt = false;
        vm.datePickerOpenStatus.updatedAt = false;
        vm.datePickerOpenStatus.deletedAt = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
