(function() {
    'use strict';
    angular
        .module('geofaraApp')
        .factory('PoiCollection', PoiCollection);

    PoiCollection.$inject = ['$resource', 'DateUtils'];

    function PoiCollection ($resource, DateUtils) {
        var resourceUrl =  'api/poi-collections/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.createdAt = DateUtils.convertDateTimeFromServer(data.createdAt);
                        data.updatedAt = DateUtils.convertDateTimeFromServer(data.updatedAt);
                        data.deletedAt = DateUtils.convertDateTimeFromServer(data.deletedAt);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
