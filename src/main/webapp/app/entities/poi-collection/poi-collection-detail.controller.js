(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('PoiCollectionDetailController', PoiCollectionDetailController);

    PoiCollectionDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'PoiCollection', 'User', 'UserGroup'];

    function PoiCollectionDetailController($scope, $rootScope, $stateParams, previousState, entity, PoiCollection, User, UserGroup) {
        var vm = this;

        vm.poiCollection = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('geofaraApp:poiCollectionUpdate', function(event, result) {
            vm.poiCollection = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
