(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('PoiCollectionDeleteController',PoiCollectionDeleteController);

    PoiCollectionDeleteController.$inject = ['$uibModalInstance', 'entity', 'PoiCollection'];

    function PoiCollectionDeleteController($uibModalInstance, entity, PoiCollection) {
        var vm = this;

        vm.poiCollection = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            PoiCollection.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
