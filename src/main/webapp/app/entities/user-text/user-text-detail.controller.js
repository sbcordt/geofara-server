(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('UserTextDetailController', UserTextDetailController);

    UserTextDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'UserText', 'User'];

    function UserTextDetailController($scope, $rootScope, $stateParams, previousState, entity, UserText, User) {
        var vm = this;

        vm.userText = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('geofaraApp:userTextUpdate', function(event, result) {
            vm.userText = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
