(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('UserTextDeleteController',UserTextDeleteController);

    UserTextDeleteController.$inject = ['$uibModalInstance', 'entity', 'UserText'];

    function UserTextDeleteController($uibModalInstance, entity, UserText) {
        var vm = this;

        vm.userText = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            UserText.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
