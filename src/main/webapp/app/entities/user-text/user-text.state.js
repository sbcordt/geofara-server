(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('user-text', {
            parent: 'entity',
            url: '/user-text',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'geofaraApp.userText.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-text/user-texts.html',
                    controller: 'UserTextController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userText');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('user-text-detail', {
            parent: 'user-text',
            url: '/user-text/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'geofaraApp.userText.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/user-text/user-text-detail.html',
                    controller: 'UserTextDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('userText');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'UserText', function($stateParams, UserText) {
                    return UserText.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'user-text',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('user-text-detail.edit', {
            parent: 'user-text-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-text/user-text-dialog.html',
                    controller: 'UserTextDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserText', function(UserText) {
                            return UserText.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-text.new', {
            parent: 'user-text',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-text/user-text-dialog.html',
                    controller: 'UserTextDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                title: null,
                                text: null,
                                latitude: null,
                                longitude: null,
                                createdAt: null,
                                updatedAt: null,
                                deletedAt: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('user-text', null, { reload: 'user-text' });
                }, function() {
                    $state.go('user-text');
                });
            }]
        })
        .state('user-text.edit', {
            parent: 'user-text',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-text/user-text-dialog.html',
                    controller: 'UserTextDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['UserText', function(UserText) {
                            return UserText.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-text', null, { reload: 'user-text' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('user-text.delete', {
            parent: 'user-text',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/user-text/user-text-delete-dialog.html',
                    controller: 'UserTextDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['UserText', function(UserText) {
                            return UserText.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('user-text', null, { reload: 'user-text' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
