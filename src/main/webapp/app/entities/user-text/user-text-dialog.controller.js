(function() {
    'use strict';

    angular
        .module('geofaraApp')
        .controller('UserTextDialogController', UserTextDialogController);

    UserTextDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'UserText', 'User'];

    function UserTextDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, UserText, User) {
        var vm = this;

        vm.userText = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.users = User.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.userText.id !== null) {
                UserText.update(vm.userText, onSaveSuccess, onSaveError);
            } else {
                UserText.save(vm.userText, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('geofaraApp:userTextUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.createdAt = false;
        vm.datePickerOpenStatus.updatedAt = false;
        vm.datePickerOpenStatus.deletedAt = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
