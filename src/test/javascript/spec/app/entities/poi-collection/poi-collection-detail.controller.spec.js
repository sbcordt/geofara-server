'use strict';

describe('Controller Tests', function() {

    describe('PoiCollection Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockPoiCollection, MockUser, MockUserGroup;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockPoiCollection = jasmine.createSpy('MockPoiCollection');
            MockUser = jasmine.createSpy('MockUser');
            MockUserGroup = jasmine.createSpy('MockUserGroup');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'PoiCollection': MockPoiCollection,
                'User': MockUser,
                'UserGroup': MockUserGroup
            };
            createController = function() {
                $injector.get('$controller')("PoiCollectionDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'geofaraApp:poiCollectionUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
