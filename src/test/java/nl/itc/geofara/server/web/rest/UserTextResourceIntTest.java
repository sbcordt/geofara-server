package nl.itc.geofara.server.web.rest;

import nl.itc.geofara.server.GeofaraApp;

import nl.itc.geofara.server.domain.UserText;
import nl.itc.geofara.server.repository.UserTextRepository;
import nl.itc.geofara.server.service.UserTextService;
import nl.itc.geofara.server.service.dto.UserTextDTO;
import nl.itc.geofara.server.service.mapper.UserTextMapper;
import nl.itc.geofara.server.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static nl.itc.geofara.server.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserTextResource REST controller.
 *
 * @see UserTextResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GeofaraApp.class)
public class UserTextResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_TEXT = "BBBBBBBBBB";

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_DELETED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DELETED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private UserTextRepository userTextRepository;

    @Autowired
    private UserTextMapper userTextMapper;

    @Autowired
    private UserTextService userTextService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUserTextMockMvc;

    private UserText userText;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UserTextResource userTextResource = new UserTextResource(userTextService);
        this.restUserTextMockMvc = MockMvcBuilders.standaloneSetup(userTextResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserText createEntity(EntityManager em) {
        UserText userText = new UserText()
            .title(DEFAULT_TITLE)
            .text(DEFAULT_TEXT)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deletedAt(DEFAULT_DELETED_AT);
        return userText;
    }

    @Before
    public void initTest() {
        userText = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserText() throws Exception {
        int databaseSizeBeforeCreate = userTextRepository.findAll().size();

        // Create the UserText
        UserTextDTO userTextDTO = userTextMapper.toDto(userText);
        restUserTextMockMvc.perform(post("/api/user-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userTextDTO)))
            .andExpect(status().isCreated());

        // Validate the UserText in the database
        List<UserText> userTextList = userTextRepository.findAll();
        assertThat(userTextList).hasSize(databaseSizeBeforeCreate + 1);
        UserText testUserText = userTextList.get(userTextList.size() - 1);
        assertThat(testUserText.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testUserText.getText()).isEqualTo(DEFAULT_TEXT);
        assertThat(testUserText.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testUserText.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testUserText.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testUserText.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testUserText.getDeletedAt()).isEqualTo(DEFAULT_DELETED_AT);
    }

    @Test
    @Transactional
    public void createUserTextWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userTextRepository.findAll().size();

        // Create the UserText with an existing ID
        userText.setId(1L);
        UserTextDTO userTextDTO = userTextMapper.toDto(userText);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserTextMockMvc.perform(post("/api/user-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userTextDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<UserText> userTextList = userTextRepository.findAll();
        assertThat(userTextList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = userTextRepository.findAll().size();
        // set the field null
        userText.setTitle(null);

        // Create the UserText, which fails.
        UserTextDTO userTextDTO = userTextMapper.toDto(userText);

        restUserTextMockMvc.perform(post("/api/user-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userTextDTO)))
            .andExpect(status().isBadRequest());

        List<UserText> userTextList = userTextRepository.findAll();
        assertThat(userTextList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLatitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = userTextRepository.findAll().size();
        // set the field null
        userText.setLatitude(null);

        // Create the UserText, which fails.
        UserTextDTO userTextDTO = userTextMapper.toDto(userText);

        restUserTextMockMvc.perform(post("/api/user-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userTextDTO)))
            .andExpect(status().isBadRequest());

        List<UserText> userTextList = userTextRepository.findAll();
        assertThat(userTextList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLongitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = userTextRepository.findAll().size();
        // set the field null
        userText.setLongitude(null);

        // Create the UserText, which fails.
        UserTextDTO userTextDTO = userTextMapper.toDto(userText);

        restUserTextMockMvc.perform(post("/api/user-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userTextDTO)))
            .andExpect(status().isBadRequest());

        List<UserText> userTextList = userTextRepository.findAll();
        assertThat(userTextList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = userTextRepository.findAll().size();
        // set the field null
        userText.setCreatedAt(null);

        // Create the UserText, which fails.
        UserTextDTO userTextDTO = userTextMapper.toDto(userText);

        restUserTextMockMvc.perform(post("/api/user-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userTextDTO)))
            .andExpect(status().isBadRequest());

        List<UserText> userTextList = userTextRepository.findAll();
        assertThat(userTextList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUpdatedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = userTextRepository.findAll().size();
        // set the field null
        userText.setUpdatedAt(null);

        // Create the UserText, which fails.
        UserTextDTO userTextDTO = userTextMapper.toDto(userText);

        restUserTextMockMvc.perform(post("/api/user-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userTextDTO)))
            .andExpect(status().isBadRequest());

        List<UserText> userTextList = userTextRepository.findAll();
        assertThat(userTextList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUserTexts() throws Exception {
        // Initialize the database
        userTextRepository.saveAndFlush(userText);

        // Get all the userTextList
        restUserTextMockMvc.perform(get("/api/user-texts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userText.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT.toString())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deletedAt").value(hasItem(sameInstant(DEFAULT_DELETED_AT))));
    }

    @Test
    @Transactional
    public void getUserText() throws Exception {
        // Initialize the database
        userTextRepository.saveAndFlush(userText);

        // Get the userText
        restUserTextMockMvc.perform(get("/api/user-texts/{id}", userText.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userText.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.text").value(DEFAULT_TEXT.toString()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deletedAt").value(sameInstant(DEFAULT_DELETED_AT)));
    }

    @Test
    @Transactional
    public void getNonExistingUserText() throws Exception {
        // Get the userText
        restUserTextMockMvc.perform(get("/api/user-texts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserText() throws Exception {
        // Initialize the database
        userTextRepository.saveAndFlush(userText);
        int databaseSizeBeforeUpdate = userTextRepository.findAll().size();

        // Update the userText
        UserText updatedUserText = userTextRepository.findOne(userText.getId());
        updatedUserText
            .title(UPDATED_TITLE)
            .text(UPDATED_TEXT)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deletedAt(UPDATED_DELETED_AT);
        UserTextDTO userTextDTO = userTextMapper.toDto(updatedUserText);

        restUserTextMockMvc.perform(put("/api/user-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userTextDTO)))
            .andExpect(status().isOk());

        // Validate the UserText in the database
        List<UserText> userTextList = userTextRepository.findAll();
        assertThat(userTextList).hasSize(databaseSizeBeforeUpdate);
        UserText testUserText = userTextList.get(userTextList.size() - 1);
        assertThat(testUserText.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testUserText.getText()).isEqualTo(UPDATED_TEXT);
        assertThat(testUserText.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testUserText.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testUserText.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testUserText.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testUserText.getDeletedAt()).isEqualTo(UPDATED_DELETED_AT);
    }

    @Test
    @Transactional
    public void updateNonExistingUserText() throws Exception {
        int databaseSizeBeforeUpdate = userTextRepository.findAll().size();

        // Create the UserText
        UserTextDTO userTextDTO = userTextMapper.toDto(userText);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUserTextMockMvc.perform(put("/api/user-texts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userTextDTO)))
            .andExpect(status().isCreated());

        // Validate the UserText in the database
        List<UserText> userTextList = userTextRepository.findAll();
        assertThat(userTextList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUserText() throws Exception {
        // Initialize the database
        userTextRepository.saveAndFlush(userText);
        int databaseSizeBeforeDelete = userTextRepository.findAll().size();

        // Get the userText
        restUserTextMockMvc.perform(delete("/api/user-texts/{id}", userText.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UserText> userTextList = userTextRepository.findAll();
        assertThat(userTextList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserText.class);
        UserText userText1 = new UserText();
        userText1.setId(1L);
        UserText userText2 = new UserText();
        userText2.setId(userText1.getId());
        assertThat(userText1).isEqualTo(userText2);
        userText2.setId(2L);
        assertThat(userText1).isNotEqualTo(userText2);
        userText1.setId(null);
        assertThat(userText1).isNotEqualTo(userText2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserTextDTO.class);
        UserTextDTO userTextDTO1 = new UserTextDTO();
        userTextDTO1.setId(1L);
        UserTextDTO userTextDTO2 = new UserTextDTO();
        assertThat(userTextDTO1).isNotEqualTo(userTextDTO2);
        userTextDTO2.setId(userTextDTO1.getId());
        assertThat(userTextDTO1).isEqualTo(userTextDTO2);
        userTextDTO2.setId(2L);
        assertThat(userTextDTO1).isNotEqualTo(userTextDTO2);
        userTextDTO1.setId(null);
        assertThat(userTextDTO1).isNotEqualTo(userTextDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(userTextMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(userTextMapper.fromId(null)).isNull();
    }
}
