package nl.itc.geofara.server.web.rest;

import nl.itc.geofara.server.GeofaraApp;

import nl.itc.geofara.server.domain.UserActivity;
import nl.itc.geofara.server.repository.UserActivityRepository;
import nl.itc.geofara.server.service.UserActivityService;
import nl.itc.geofara.server.service.dto.UserActivityDTO;
import nl.itc.geofara.server.service.mapper.UserActivityMapper;
import nl.itc.geofara.server.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static nl.itc.geofara.server.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserActivityResource REST controller.
 *
 * @see UserActivityResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GeofaraApp.class)
public class UserActivityResourceIntTest {

    private static final String DEFAULT_ACTIVITY = "AAAAAAAAAA";
    private static final String UPDATED_ACTIVITY = "BBBBBBBBBB";

    private static final Integer DEFAULT_CONFIDENCE = 1;
    private static final Integer UPDATED_CONFIDENCE = 2;

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;

    private static final Double DEFAULT_ACCURACY = 1D;
    private static final Double UPDATED_ACCURACY = 2D;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_DELETED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DELETED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private UserActivityRepository userActivityRepository;

    @Autowired
    private UserActivityMapper userActivityMapper;

    @Autowired
    private UserActivityService userActivityService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUserActivityMockMvc;

    private UserActivity userActivity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UserActivityResource userActivityResource = new UserActivityResource(userActivityService);
        this.restUserActivityMockMvc = MockMvcBuilders.standaloneSetup(userActivityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserActivity createEntity(EntityManager em) {
        UserActivity userActivity = new UserActivity()
            .activity(DEFAULT_ACTIVITY)
            .confidence(DEFAULT_CONFIDENCE)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .accuracy(DEFAULT_ACCURACY)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deletedAt(DEFAULT_DELETED_AT);
        return userActivity;
    }

    @Before
    public void initTest() {
        userActivity = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserActivity() throws Exception {
        int databaseSizeBeforeCreate = userActivityRepository.findAll().size();

        // Create the UserActivity
        UserActivityDTO userActivityDTO = userActivityMapper.toDto(userActivity);
        restUserActivityMockMvc.perform(post("/api/user-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActivityDTO)))
            .andExpect(status().isCreated());

        // Validate the UserActivity in the database
        List<UserActivity> userActivityList = userActivityRepository.findAll();
        assertThat(userActivityList).hasSize(databaseSizeBeforeCreate + 1);
        UserActivity testUserActivity = userActivityList.get(userActivityList.size() - 1);
        assertThat(testUserActivity.getActivity()).isEqualTo(DEFAULT_ACTIVITY);
        assertThat(testUserActivity.getConfidence()).isEqualTo(DEFAULT_CONFIDENCE);
        assertThat(testUserActivity.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testUserActivity.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testUserActivity.getAccuracy()).isEqualTo(DEFAULT_ACCURACY);
        assertThat(testUserActivity.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testUserActivity.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testUserActivity.getDeletedAt()).isEqualTo(DEFAULT_DELETED_AT);
    }

    @Test
    @Transactional
    public void createUserActivityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userActivityRepository.findAll().size();

        // Create the UserActivity with an existing ID
        userActivity.setId(1L);
        UserActivityDTO userActivityDTO = userActivityMapper.toDto(userActivity);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserActivityMockMvc.perform(post("/api/user-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActivityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<UserActivity> userActivityList = userActivityRepository.findAll();
        assertThat(userActivityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkActivityIsRequired() throws Exception {
        int databaseSizeBeforeTest = userActivityRepository.findAll().size();
        // set the field null
        userActivity.setActivity(null);

        // Create the UserActivity, which fails.
        UserActivityDTO userActivityDTO = userActivityMapper.toDto(userActivity);

        restUserActivityMockMvc.perform(post("/api/user-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActivityDTO)))
            .andExpect(status().isBadRequest());

        List<UserActivity> userActivityList = userActivityRepository.findAll();
        assertThat(userActivityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkConfidenceIsRequired() throws Exception {
        int databaseSizeBeforeTest = userActivityRepository.findAll().size();
        // set the field null
        userActivity.setConfidence(null);

        // Create the UserActivity, which fails.
        UserActivityDTO userActivityDTO = userActivityMapper.toDto(userActivity);

        restUserActivityMockMvc.perform(post("/api/user-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActivityDTO)))
            .andExpect(status().isBadRequest());

        List<UserActivity> userActivityList = userActivityRepository.findAll();
        assertThat(userActivityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLatitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = userActivityRepository.findAll().size();
        // set the field null
        userActivity.setLatitude(null);

        // Create the UserActivity, which fails.
        UserActivityDTO userActivityDTO = userActivityMapper.toDto(userActivity);

        restUserActivityMockMvc.perform(post("/api/user-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActivityDTO)))
            .andExpect(status().isBadRequest());

        List<UserActivity> userActivityList = userActivityRepository.findAll();
        assertThat(userActivityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLongitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = userActivityRepository.findAll().size();
        // set the field null
        userActivity.setLongitude(null);

        // Create the UserActivity, which fails.
        UserActivityDTO userActivityDTO = userActivityMapper.toDto(userActivity);

        restUserActivityMockMvc.perform(post("/api/user-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActivityDTO)))
            .andExpect(status().isBadRequest());

        List<UserActivity> userActivityList = userActivityRepository.findAll();
        assertThat(userActivityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAccuracyIsRequired() throws Exception {
        int databaseSizeBeforeTest = userActivityRepository.findAll().size();
        // set the field null
        userActivity.setAccuracy(null);

        // Create the UserActivity, which fails.
        UserActivityDTO userActivityDTO = userActivityMapper.toDto(userActivity);

        restUserActivityMockMvc.perform(post("/api/user-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActivityDTO)))
            .andExpect(status().isBadRequest());

        List<UserActivity> userActivityList = userActivityRepository.findAll();
        assertThat(userActivityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = userActivityRepository.findAll().size();
        // set the field null
        userActivity.setCreatedAt(null);

        // Create the UserActivity, which fails.
        UserActivityDTO userActivityDTO = userActivityMapper.toDto(userActivity);

        restUserActivityMockMvc.perform(post("/api/user-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActivityDTO)))
            .andExpect(status().isBadRequest());

        List<UserActivity> userActivityList = userActivityRepository.findAll();
        assertThat(userActivityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUpdatedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = userActivityRepository.findAll().size();
        // set the field null
        userActivity.setUpdatedAt(null);

        // Create the UserActivity, which fails.
        UserActivityDTO userActivityDTO = userActivityMapper.toDto(userActivity);

        restUserActivityMockMvc.perform(post("/api/user-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActivityDTO)))
            .andExpect(status().isBadRequest());

        List<UserActivity> userActivityList = userActivityRepository.findAll();
        assertThat(userActivityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUserActivities() throws Exception {
        // Initialize the database
        userActivityRepository.saveAndFlush(userActivity);

        // Get all the userActivityList
        restUserActivityMockMvc.perform(get("/api/user-activities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userActivity.getId().intValue())))
            .andExpect(jsonPath("$.[*].activity").value(hasItem(DEFAULT_ACTIVITY.toString())))
            .andExpect(jsonPath("$.[*].confidence").value(hasItem(DEFAULT_CONFIDENCE)))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].accuracy").value(hasItem(DEFAULT_ACCURACY.doubleValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deletedAt").value(hasItem(sameInstant(DEFAULT_DELETED_AT))));
    }

    @Test
    @Transactional
    public void getUserActivity() throws Exception {
        // Initialize the database
        userActivityRepository.saveAndFlush(userActivity);

        // Get the userActivity
        restUserActivityMockMvc.perform(get("/api/user-activities/{id}", userActivity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userActivity.getId().intValue()))
            .andExpect(jsonPath("$.activity").value(DEFAULT_ACTIVITY.toString()))
            .andExpect(jsonPath("$.confidence").value(DEFAULT_CONFIDENCE))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.accuracy").value(DEFAULT_ACCURACY.doubleValue()))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deletedAt").value(sameInstant(DEFAULT_DELETED_AT)));
    }

    @Test
    @Transactional
    public void getNonExistingUserActivity() throws Exception {
        // Get the userActivity
        restUserActivityMockMvc.perform(get("/api/user-activities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserActivity() throws Exception {
        // Initialize the database
        userActivityRepository.saveAndFlush(userActivity);
        int databaseSizeBeforeUpdate = userActivityRepository.findAll().size();

        // Update the userActivity
        UserActivity updatedUserActivity = userActivityRepository.findOne(userActivity.getId());
        updatedUserActivity
            .activity(UPDATED_ACTIVITY)
            .confidence(UPDATED_CONFIDENCE)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .accuracy(UPDATED_ACCURACY)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deletedAt(UPDATED_DELETED_AT);
        UserActivityDTO userActivityDTO = userActivityMapper.toDto(updatedUserActivity);

        restUserActivityMockMvc.perform(put("/api/user-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActivityDTO)))
            .andExpect(status().isOk());

        // Validate the UserActivity in the database
        List<UserActivity> userActivityList = userActivityRepository.findAll();
        assertThat(userActivityList).hasSize(databaseSizeBeforeUpdate);
        UserActivity testUserActivity = userActivityList.get(userActivityList.size() - 1);
        assertThat(testUserActivity.getActivity()).isEqualTo(UPDATED_ACTIVITY);
        assertThat(testUserActivity.getConfidence()).isEqualTo(UPDATED_CONFIDENCE);
        assertThat(testUserActivity.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testUserActivity.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testUserActivity.getAccuracy()).isEqualTo(UPDATED_ACCURACY);
        assertThat(testUserActivity.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testUserActivity.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testUserActivity.getDeletedAt()).isEqualTo(UPDATED_DELETED_AT);
    }

    @Test
    @Transactional
    public void updateNonExistingUserActivity() throws Exception {
        int databaseSizeBeforeUpdate = userActivityRepository.findAll().size();

        // Create the UserActivity
        UserActivityDTO userActivityDTO = userActivityMapper.toDto(userActivity);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUserActivityMockMvc.perform(put("/api/user-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActivityDTO)))
            .andExpect(status().isCreated());

        // Validate the UserActivity in the database
        List<UserActivity> userActivityList = userActivityRepository.findAll();
        assertThat(userActivityList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUserActivity() throws Exception {
        // Initialize the database
        userActivityRepository.saveAndFlush(userActivity);
        int databaseSizeBeforeDelete = userActivityRepository.findAll().size();

        // Get the userActivity
        restUserActivityMockMvc.perform(delete("/api/user-activities/{id}", userActivity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UserActivity> userActivityList = userActivityRepository.findAll();
        assertThat(userActivityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserActivity.class);
        UserActivity userActivity1 = new UserActivity();
        userActivity1.setId(1L);
        UserActivity userActivity2 = new UserActivity();
        userActivity2.setId(userActivity1.getId());
        assertThat(userActivity1).isEqualTo(userActivity2);
        userActivity2.setId(2L);
        assertThat(userActivity1).isNotEqualTo(userActivity2);
        userActivity1.setId(null);
        assertThat(userActivity1).isNotEqualTo(userActivity2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserActivityDTO.class);
        UserActivityDTO userActivityDTO1 = new UserActivityDTO();
        userActivityDTO1.setId(1L);
        UserActivityDTO userActivityDTO2 = new UserActivityDTO();
        assertThat(userActivityDTO1).isNotEqualTo(userActivityDTO2);
        userActivityDTO2.setId(userActivityDTO1.getId());
        assertThat(userActivityDTO1).isEqualTo(userActivityDTO2);
        userActivityDTO2.setId(2L);
        assertThat(userActivityDTO1).isNotEqualTo(userActivityDTO2);
        userActivityDTO1.setId(null);
        assertThat(userActivityDTO1).isNotEqualTo(userActivityDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(userActivityMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(userActivityMapper.fromId(null)).isNull();
    }
}
