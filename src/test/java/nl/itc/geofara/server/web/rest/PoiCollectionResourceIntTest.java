package nl.itc.geofara.server.web.rest;

import nl.itc.geofara.server.GeofaraApp;

import nl.itc.geofara.server.domain.PoiCollection;
import nl.itc.geofara.server.repository.PoiCollectionRepository;
import nl.itc.geofara.server.service.PoiCollectionService;
import nl.itc.geofara.server.service.dto.PoiCollectionDTO;
import nl.itc.geofara.server.service.mapper.PoiCollectionMapper;
import nl.itc.geofara.server.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static nl.itc.geofara.server.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PoiCollectionResource REST controller.
 *
 * @see PoiCollectionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GeofaraApp.class)
public class PoiCollectionResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_DELETED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DELETED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private PoiCollectionRepository poiCollectionRepository;

    @Autowired
    private PoiCollectionMapper poiCollectionMapper;

    @Autowired
    private PoiCollectionService poiCollectionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPoiCollectionMockMvc;

    private PoiCollection poiCollection;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PoiCollectionResource poiCollectionResource = new PoiCollectionResource(poiCollectionService);
        this.restPoiCollectionMockMvc = MockMvcBuilders.standaloneSetup(poiCollectionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PoiCollection createEntity(EntityManager em) {
        PoiCollection poiCollection = new PoiCollection()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deletedAt(DEFAULT_DELETED_AT);
        return poiCollection;
    }

    @Before
    public void initTest() {
        poiCollection = createEntity(em);
    }

    @Test
    @Transactional
    public void createPoiCollection() throws Exception {
        int databaseSizeBeforeCreate = poiCollectionRepository.findAll().size();

        // Create the PoiCollection
        PoiCollectionDTO poiCollectionDTO = poiCollectionMapper.toDto(poiCollection);
        restPoiCollectionMockMvc.perform(post("/api/poi-collections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(poiCollectionDTO)))
            .andExpect(status().isCreated());

        // Validate the PoiCollection in the database
        List<PoiCollection> poiCollectionList = poiCollectionRepository.findAll();
        assertThat(poiCollectionList).hasSize(databaseSizeBeforeCreate + 1);
        PoiCollection testPoiCollection = poiCollectionList.get(poiCollectionList.size() - 1);
        assertThat(testPoiCollection.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPoiCollection.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPoiCollection.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPoiCollection.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testPoiCollection.getDeletedAt()).isEqualTo(DEFAULT_DELETED_AT);
    }

    @Test
    @Transactional
    public void createPoiCollectionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = poiCollectionRepository.findAll().size();

        // Create the PoiCollection with an existing ID
        poiCollection.setId(1L);
        PoiCollectionDTO poiCollectionDTO = poiCollectionMapper.toDto(poiCollection);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPoiCollectionMockMvc.perform(post("/api/poi-collections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(poiCollectionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PoiCollection> poiCollectionList = poiCollectionRepository.findAll();
        assertThat(poiCollectionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = poiCollectionRepository.findAll().size();
        // set the field null
        poiCollection.setName(null);

        // Create the PoiCollection, which fails.
        PoiCollectionDTO poiCollectionDTO = poiCollectionMapper.toDto(poiCollection);

        restPoiCollectionMockMvc.perform(post("/api/poi-collections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(poiCollectionDTO)))
            .andExpect(status().isBadRequest());

        List<PoiCollection> poiCollectionList = poiCollectionRepository.findAll();
        assertThat(poiCollectionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = poiCollectionRepository.findAll().size();
        // set the field null
        poiCollection.setCreatedAt(null);

        // Create the PoiCollection, which fails.
        PoiCollectionDTO poiCollectionDTO = poiCollectionMapper.toDto(poiCollection);

        restPoiCollectionMockMvc.perform(post("/api/poi-collections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(poiCollectionDTO)))
            .andExpect(status().isBadRequest());

        List<PoiCollection> poiCollectionList = poiCollectionRepository.findAll();
        assertThat(poiCollectionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUpdatedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = poiCollectionRepository.findAll().size();
        // set the field null
        poiCollection.setUpdatedAt(null);

        // Create the PoiCollection, which fails.
        PoiCollectionDTO poiCollectionDTO = poiCollectionMapper.toDto(poiCollection);

        restPoiCollectionMockMvc.perform(post("/api/poi-collections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(poiCollectionDTO)))
            .andExpect(status().isBadRequest());

        List<PoiCollection> poiCollectionList = poiCollectionRepository.findAll();
        assertThat(poiCollectionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPoiCollections() throws Exception {
        // Initialize the database
        poiCollectionRepository.saveAndFlush(poiCollection);

        // Get all the poiCollectionList
        restPoiCollectionMockMvc.perform(get("/api/poi-collections?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(poiCollection.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deletedAt").value(hasItem(sameInstant(DEFAULT_DELETED_AT))));
    }

    @Test
    @Transactional
    public void getPoiCollection() throws Exception {
        // Initialize the database
        poiCollectionRepository.saveAndFlush(poiCollection);

        // Get the poiCollection
        restPoiCollectionMockMvc.perform(get("/api/poi-collections/{id}", poiCollection.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(poiCollection.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deletedAt").value(sameInstant(DEFAULT_DELETED_AT)));
    }

    @Test
    @Transactional
    public void getNonExistingPoiCollection() throws Exception {
        // Get the poiCollection
        restPoiCollectionMockMvc.perform(get("/api/poi-collections/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePoiCollection() throws Exception {
        // Initialize the database
        poiCollectionRepository.saveAndFlush(poiCollection);
        int databaseSizeBeforeUpdate = poiCollectionRepository.findAll().size();

        // Update the poiCollection
        PoiCollection updatedPoiCollection = poiCollectionRepository.findOne(poiCollection.getId());
        updatedPoiCollection
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deletedAt(UPDATED_DELETED_AT);
        PoiCollectionDTO poiCollectionDTO = poiCollectionMapper.toDto(updatedPoiCollection);

        restPoiCollectionMockMvc.perform(put("/api/poi-collections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(poiCollectionDTO)))
            .andExpect(status().isOk());

        // Validate the PoiCollection in the database
        List<PoiCollection> poiCollectionList = poiCollectionRepository.findAll();
        assertThat(poiCollectionList).hasSize(databaseSizeBeforeUpdate);
        PoiCollection testPoiCollection = poiCollectionList.get(poiCollectionList.size() - 1);
        assertThat(testPoiCollection.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPoiCollection.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPoiCollection.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPoiCollection.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testPoiCollection.getDeletedAt()).isEqualTo(UPDATED_DELETED_AT);
    }

    @Test
    @Transactional
    public void updateNonExistingPoiCollection() throws Exception {
        int databaseSizeBeforeUpdate = poiCollectionRepository.findAll().size();

        // Create the PoiCollection
        PoiCollectionDTO poiCollectionDTO = poiCollectionMapper.toDto(poiCollection);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPoiCollectionMockMvc.perform(put("/api/poi-collections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(poiCollectionDTO)))
            .andExpect(status().isCreated());

        // Validate the PoiCollection in the database
        List<PoiCollection> poiCollectionList = poiCollectionRepository.findAll();
        assertThat(poiCollectionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePoiCollection() throws Exception {
        // Initialize the database
        poiCollectionRepository.saveAndFlush(poiCollection);
        int databaseSizeBeforeDelete = poiCollectionRepository.findAll().size();

        // Get the poiCollection
        restPoiCollectionMockMvc.perform(delete("/api/poi-collections/{id}", poiCollection.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PoiCollection> poiCollectionList = poiCollectionRepository.findAll();
        assertThat(poiCollectionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PoiCollection.class);
        PoiCollection poiCollection1 = new PoiCollection();
        poiCollection1.setId(1L);
        PoiCollection poiCollection2 = new PoiCollection();
        poiCollection2.setId(poiCollection1.getId());
        assertThat(poiCollection1).isEqualTo(poiCollection2);
        poiCollection2.setId(2L);
        assertThat(poiCollection1).isNotEqualTo(poiCollection2);
        poiCollection1.setId(null);
        assertThat(poiCollection1).isNotEqualTo(poiCollection2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PoiCollectionDTO.class);
        PoiCollectionDTO poiCollectionDTO1 = new PoiCollectionDTO();
        poiCollectionDTO1.setId(1L);
        PoiCollectionDTO poiCollectionDTO2 = new PoiCollectionDTO();
        assertThat(poiCollectionDTO1).isNotEqualTo(poiCollectionDTO2);
        poiCollectionDTO2.setId(poiCollectionDTO1.getId());
        assertThat(poiCollectionDTO1).isEqualTo(poiCollectionDTO2);
        poiCollectionDTO2.setId(2L);
        assertThat(poiCollectionDTO1).isNotEqualTo(poiCollectionDTO2);
        poiCollectionDTO1.setId(null);
        assertThat(poiCollectionDTO1).isNotEqualTo(poiCollectionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(poiCollectionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(poiCollectionMapper.fromId(null)).isNull();
    }
}
