package nl.itc.geofara.server.web.rest;

import nl.itc.geofara.server.GeofaraApp;

import nl.itc.geofara.server.domain.UserAction;
import nl.itc.geofara.server.repository.UserActionRepository;
import nl.itc.geofara.server.service.UserActionService;
import nl.itc.geofara.server.service.dto.UserActionDTO;
import nl.itc.geofara.server.service.mapper.UserActionMapper;
import nl.itc.geofara.server.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static nl.itc.geofara.server.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserActionResource REST controller.
 *
 * @see UserActionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GeofaraApp.class)
public class UserActionResourceIntTest {

    private static final String DEFAULT_TAG = "AAAAAAAAAA";
    private static final String UPDATED_TAG = "BBBBBBBBBB";

    private static final String DEFAULT_VIEW_GROUP = "AAAAAAAAAA";
    private static final String UPDATED_VIEW_GROUP = "BBBBBBBBBB";

    private static final String DEFAULT_ROOT_VIEW = "AAAAAAAAAA";
    private static final String UPDATED_ROOT_VIEW = "BBBBBBBBBB";

    private static final String DEFAULT_ACTION = "AAAAAAAAAA";
    private static final String UPDATED_ACTION = "BBBBBBBBBB";

    private static final Double DEFAULT_INPUT_X = 1D;
    private static final Double UPDATED_INPUT_X = 2D;

    private static final Double DEFAULT_INPUT_Y = 1D;
    private static final Double UPDATED_INPUT_Y = 2D;

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_DELETED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DELETED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private UserActionRepository userActionRepository;

    @Autowired
    private UserActionMapper userActionMapper;

    @Autowired
    private UserActionService userActionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUserActionMockMvc;

    private UserAction userAction;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UserActionResource userActionResource = new UserActionResource(userActionService);
        this.restUserActionMockMvc = MockMvcBuilders.standaloneSetup(userActionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserAction createEntity(EntityManager em) {
        UserAction userAction = new UserAction()
            .tag(DEFAULT_TAG)
            .viewGroup(DEFAULT_VIEW_GROUP)
            .rootView(DEFAULT_ROOT_VIEW)
            .action(DEFAULT_ACTION)
            .inputX(DEFAULT_INPUT_X)
            .inputY(DEFAULT_INPUT_Y)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deletedAt(DEFAULT_DELETED_AT);
        return userAction;
    }

    @Before
    public void initTest() {
        userAction = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserAction() throws Exception {
        int databaseSizeBeforeCreate = userActionRepository.findAll().size();

        // Create the UserAction
        UserActionDTO userActionDTO = userActionMapper.toDto(userAction);
        restUserActionMockMvc.perform(post("/api/user-actions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActionDTO)))
            .andExpect(status().isCreated());

        // Validate the UserAction in the database
        List<UserAction> userActionList = userActionRepository.findAll();
        assertThat(userActionList).hasSize(databaseSizeBeforeCreate + 1);
        UserAction testUserAction = userActionList.get(userActionList.size() - 1);
        assertThat(testUserAction.getTag()).isEqualTo(DEFAULT_TAG);
        assertThat(testUserAction.getViewGroup()).isEqualTo(DEFAULT_VIEW_GROUP);
        assertThat(testUserAction.getRootView()).isEqualTo(DEFAULT_ROOT_VIEW);
        assertThat(testUserAction.getAction()).isEqualTo(DEFAULT_ACTION);
        assertThat(testUserAction.getInputX()).isEqualTo(DEFAULT_INPUT_X);
        assertThat(testUserAction.getInputY()).isEqualTo(DEFAULT_INPUT_Y);
        assertThat(testUserAction.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testUserAction.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testUserAction.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testUserAction.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testUserAction.getDeletedAt()).isEqualTo(DEFAULT_DELETED_AT);
    }

    @Test
    @Transactional
    public void createUserActionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userActionRepository.findAll().size();

        // Create the UserAction with an existing ID
        userAction.setId(1L);
        UserActionDTO userActionDTO = userActionMapper.toDto(userAction);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserActionMockMvc.perform(post("/api/user-actions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<UserAction> userActionList = userActionRepository.findAll();
        assertThat(userActionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkLatitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = userActionRepository.findAll().size();
        // set the field null
        userAction.setLatitude(null);

        // Create the UserAction, which fails.
        UserActionDTO userActionDTO = userActionMapper.toDto(userAction);

        restUserActionMockMvc.perform(post("/api/user-actions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActionDTO)))
            .andExpect(status().isBadRequest());

        List<UserAction> userActionList = userActionRepository.findAll();
        assertThat(userActionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLongitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = userActionRepository.findAll().size();
        // set the field null
        userAction.setLongitude(null);

        // Create the UserAction, which fails.
        UserActionDTO userActionDTO = userActionMapper.toDto(userAction);

        restUserActionMockMvc.perform(post("/api/user-actions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActionDTO)))
            .andExpect(status().isBadRequest());

        List<UserAction> userActionList = userActionRepository.findAll();
        assertThat(userActionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = userActionRepository.findAll().size();
        // set the field null
        userAction.setCreatedAt(null);

        // Create the UserAction, which fails.
        UserActionDTO userActionDTO = userActionMapper.toDto(userAction);

        restUserActionMockMvc.perform(post("/api/user-actions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActionDTO)))
            .andExpect(status().isBadRequest());

        List<UserAction> userActionList = userActionRepository.findAll();
        assertThat(userActionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUpdatedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = userActionRepository.findAll().size();
        // set the field null
        userAction.setUpdatedAt(null);

        // Create the UserAction, which fails.
        UserActionDTO userActionDTO = userActionMapper.toDto(userAction);

        restUserActionMockMvc.perform(post("/api/user-actions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActionDTO)))
            .andExpect(status().isBadRequest());

        List<UserAction> userActionList = userActionRepository.findAll();
        assertThat(userActionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUserActions() throws Exception {
        // Initialize the database
        userActionRepository.saveAndFlush(userAction);

        // Get all the userActionList
        restUserActionMockMvc.perform(get("/api/user-actions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userAction.getId().intValue())))
            .andExpect(jsonPath("$.[*].tag").value(hasItem(DEFAULT_TAG.toString())))
            .andExpect(jsonPath("$.[*].viewGroup").value(hasItem(DEFAULT_VIEW_GROUP.toString())))
            .andExpect(jsonPath("$.[*].rootView").value(hasItem(DEFAULT_ROOT_VIEW.toString())))
            .andExpect(jsonPath("$.[*].action").value(hasItem(DEFAULT_ACTION.toString())))
            .andExpect(jsonPath("$.[*].inputX").value(hasItem(DEFAULT_INPUT_X.doubleValue())))
            .andExpect(jsonPath("$.[*].inputY").value(hasItem(DEFAULT_INPUT_Y.doubleValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deletedAt").value(hasItem(sameInstant(DEFAULT_DELETED_AT))));
    }

    @Test
    @Transactional
    public void getUserAction() throws Exception {
        // Initialize the database
        userActionRepository.saveAndFlush(userAction);

        // Get the userAction
        restUserActionMockMvc.perform(get("/api/user-actions/{id}", userAction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userAction.getId().intValue()))
            .andExpect(jsonPath("$.tag").value(DEFAULT_TAG.toString()))
            .andExpect(jsonPath("$.viewGroup").value(DEFAULT_VIEW_GROUP.toString()))
            .andExpect(jsonPath("$.rootView").value(DEFAULT_ROOT_VIEW.toString()))
            .andExpect(jsonPath("$.action").value(DEFAULT_ACTION.toString()))
            .andExpect(jsonPath("$.inputX").value(DEFAULT_INPUT_X.doubleValue()))
            .andExpect(jsonPath("$.inputY").value(DEFAULT_INPUT_Y.doubleValue()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deletedAt").value(sameInstant(DEFAULT_DELETED_AT)));
    }

    @Test
    @Transactional
    public void getNonExistingUserAction() throws Exception {
        // Get the userAction
        restUserActionMockMvc.perform(get("/api/user-actions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserAction() throws Exception {
        // Initialize the database
        userActionRepository.saveAndFlush(userAction);
        int databaseSizeBeforeUpdate = userActionRepository.findAll().size();

        // Update the userAction
        UserAction updatedUserAction = userActionRepository.findOne(userAction.getId());
        updatedUserAction
            .tag(UPDATED_TAG)
            .viewGroup(UPDATED_VIEW_GROUP)
            .rootView(UPDATED_ROOT_VIEW)
            .action(UPDATED_ACTION)
            .inputX(UPDATED_INPUT_X)
            .inputY(UPDATED_INPUT_Y)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deletedAt(UPDATED_DELETED_AT);
        UserActionDTO userActionDTO = userActionMapper.toDto(updatedUserAction);

        restUserActionMockMvc.perform(put("/api/user-actions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActionDTO)))
            .andExpect(status().isOk());

        // Validate the UserAction in the database
        List<UserAction> userActionList = userActionRepository.findAll();
        assertThat(userActionList).hasSize(databaseSizeBeforeUpdate);
        UserAction testUserAction = userActionList.get(userActionList.size() - 1);
        assertThat(testUserAction.getTag()).isEqualTo(UPDATED_TAG);
        assertThat(testUserAction.getViewGroup()).isEqualTo(UPDATED_VIEW_GROUP);
        assertThat(testUserAction.getRootView()).isEqualTo(UPDATED_ROOT_VIEW);
        assertThat(testUserAction.getAction()).isEqualTo(UPDATED_ACTION);
        assertThat(testUserAction.getInputX()).isEqualTo(UPDATED_INPUT_X);
        assertThat(testUserAction.getInputY()).isEqualTo(UPDATED_INPUT_Y);
        assertThat(testUserAction.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testUserAction.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testUserAction.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testUserAction.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testUserAction.getDeletedAt()).isEqualTo(UPDATED_DELETED_AT);
    }

    @Test
    @Transactional
    public void updateNonExistingUserAction() throws Exception {
        int databaseSizeBeforeUpdate = userActionRepository.findAll().size();

        // Create the UserAction
        UserActionDTO userActionDTO = userActionMapper.toDto(userAction);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUserActionMockMvc.perform(put("/api/user-actions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userActionDTO)))
            .andExpect(status().isCreated());

        // Validate the UserAction in the database
        List<UserAction> userActionList = userActionRepository.findAll();
        assertThat(userActionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUserAction() throws Exception {
        // Initialize the database
        userActionRepository.saveAndFlush(userAction);
        int databaseSizeBeforeDelete = userActionRepository.findAll().size();

        // Get the userAction
        restUserActionMockMvc.perform(delete("/api/user-actions/{id}", userAction.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UserAction> userActionList = userActionRepository.findAll();
        assertThat(userActionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserAction.class);
        UserAction userAction1 = new UserAction();
        userAction1.setId(1L);
        UserAction userAction2 = new UserAction();
        userAction2.setId(userAction1.getId());
        assertThat(userAction1).isEqualTo(userAction2);
        userAction2.setId(2L);
        assertThat(userAction1).isNotEqualTo(userAction2);
        userAction1.setId(null);
        assertThat(userAction1).isNotEqualTo(userAction2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserActionDTO.class);
        UserActionDTO userActionDTO1 = new UserActionDTO();
        userActionDTO1.setId(1L);
        UserActionDTO userActionDTO2 = new UserActionDTO();
        assertThat(userActionDTO1).isNotEqualTo(userActionDTO2);
        userActionDTO2.setId(userActionDTO1.getId());
        assertThat(userActionDTO1).isEqualTo(userActionDTO2);
        userActionDTO2.setId(2L);
        assertThat(userActionDTO1).isNotEqualTo(userActionDTO2);
        userActionDTO1.setId(null);
        assertThat(userActionDTO1).isNotEqualTo(userActionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(userActionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(userActionMapper.fromId(null)).isNull();
    }
}
