package nl.itc.geofara.server.web.rest;

import nl.itc.geofara.server.GeofaraApp;

import nl.itc.geofara.server.domain.UserImage;
import nl.itc.geofara.server.repository.UserImageRepository;
import nl.itc.geofara.server.service.UserImageService;
import nl.itc.geofara.server.service.dto.UserImageDTO;
import nl.itc.geofara.server.service.mapper.UserImageMapper;
import nl.itc.geofara.server.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static nl.itc.geofara.server.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import nl.itc.geofara.server.domain.enumeration.ImageType;
/**
 * Test class for the UserImageResource REST controller.
 *
 * @see UserImageResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GeofaraApp.class)
public class UserImageResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final ImageType DEFAULT_IMAGE_TYPE = ImageType.SCREENSHOT;
    private static final ImageType UPDATED_IMAGE_TYPE = ImageType.PHOTO;

    private static final byte[] DEFAULT_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_CONTENT_TYPE = "image/png";

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_DELETED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DELETED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private UserImageRepository userImageRepository;

    @Autowired
    private UserImageMapper userImageMapper;

    @Autowired
    private UserImageService userImageService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restUserImageMockMvc;

    private UserImage userImage;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        UserImageResource userImageResource = new UserImageResource(userImageService);
        this.restUserImageMockMvc = MockMvcBuilders.standaloneSetup(userImageResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserImage createEntity(EntityManager em) {
        UserImage userImage = new UserImage()
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .imageType(DEFAULT_IMAGE_TYPE)
            .image(DEFAULT_IMAGE)
            .imageContentType(DEFAULT_IMAGE_CONTENT_TYPE)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deletedAt(DEFAULT_DELETED_AT);
        return userImage;
    }

    @Before
    public void initTest() {
        userImage = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserImage() throws Exception {
        int databaseSizeBeforeCreate = userImageRepository.findAll().size();

        // Create the UserImage
        UserImageDTO userImageDTO = userImageMapper.toDto(userImage);
        restUserImageMockMvc.perform(post("/api/user-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userImageDTO)))
            .andExpect(status().isCreated());

        // Validate the UserImage in the database
        List<UserImage> userImageList = userImageRepository.findAll();
        assertThat(userImageList).hasSize(databaseSizeBeforeCreate + 1);
        UserImage testUserImage = userImageList.get(userImageList.size() - 1);
        assertThat(testUserImage.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testUserImage.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testUserImage.getImageType()).isEqualTo(DEFAULT_IMAGE_TYPE);
        assertThat(testUserImage.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testUserImage.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);
        assertThat(testUserImage.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testUserImage.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testUserImage.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testUserImage.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testUserImage.getDeletedAt()).isEqualTo(DEFAULT_DELETED_AT);
    }

    @Test
    @Transactional
    public void createUserImageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userImageRepository.findAll().size();

        // Create the UserImage with an existing ID
        userImage.setId(1L);
        UserImageDTO userImageDTO = userImageMapper.toDto(userImage);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserImageMockMvc.perform(post("/api/user-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userImageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<UserImage> userImageList = userImageRepository.findAll();
        assertThat(userImageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = userImageRepository.findAll().size();
        // set the field null
        userImage.setTitle(null);

        // Create the UserImage, which fails.
        UserImageDTO userImageDTO = userImageMapper.toDto(userImage);

        restUserImageMockMvc.perform(post("/api/user-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userImageDTO)))
            .andExpect(status().isBadRequest());

        List<UserImage> userImageList = userImageRepository.findAll();
        assertThat(userImageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkImageIsRequired() throws Exception {
        int databaseSizeBeforeTest = userImageRepository.findAll().size();
        // set the field null
        userImage.setImage(null);

        // Create the UserImage, which fails.
        UserImageDTO userImageDTO = userImageMapper.toDto(userImage);

        restUserImageMockMvc.perform(post("/api/user-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userImageDTO)))
            .andExpect(status().isBadRequest());

        List<UserImage> userImageList = userImageRepository.findAll();
        assertThat(userImageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLatitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = userImageRepository.findAll().size();
        // set the field null
        userImage.setLatitude(null);

        // Create the UserImage, which fails.
        UserImageDTO userImageDTO = userImageMapper.toDto(userImage);

        restUserImageMockMvc.perform(post("/api/user-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userImageDTO)))
            .andExpect(status().isBadRequest());

        List<UserImage> userImageList = userImageRepository.findAll();
        assertThat(userImageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLongitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = userImageRepository.findAll().size();
        // set the field null
        userImage.setLongitude(null);

        // Create the UserImage, which fails.
        UserImageDTO userImageDTO = userImageMapper.toDto(userImage);

        restUserImageMockMvc.perform(post("/api/user-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userImageDTO)))
            .andExpect(status().isBadRequest());

        List<UserImage> userImageList = userImageRepository.findAll();
        assertThat(userImageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = userImageRepository.findAll().size();
        // set the field null
        userImage.setCreatedAt(null);

        // Create the UserImage, which fails.
        UserImageDTO userImageDTO = userImageMapper.toDto(userImage);

        restUserImageMockMvc.perform(post("/api/user-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userImageDTO)))
            .andExpect(status().isBadRequest());

        List<UserImage> userImageList = userImageRepository.findAll();
        assertThat(userImageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUpdatedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = userImageRepository.findAll().size();
        // set the field null
        userImage.setUpdatedAt(null);

        // Create the UserImage, which fails.
        UserImageDTO userImageDTO = userImageMapper.toDto(userImage);

        restUserImageMockMvc.perform(post("/api/user-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userImageDTO)))
            .andExpect(status().isBadRequest());

        List<UserImage> userImageList = userImageRepository.findAll();
        assertThat(userImageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUserImages() throws Exception {
        // Initialize the database
        userImageRepository.saveAndFlush(userImage);

        // Get all the userImageList
        restUserImageMockMvc.perform(get("/api/user-images?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userImage.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].imageType").value(hasItem(DEFAULT_IMAGE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE))))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deletedAt").value(hasItem(sameInstant(DEFAULT_DELETED_AT))));
    }

    @Test
    @Transactional
    public void getUserImage() throws Exception {
        // Initialize the database
        userImageRepository.saveAndFlush(userImage);

        // Get the userImage
        restUserImageMockMvc.perform(get("/api/user-images/{id}", userImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userImage.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.imageType").value(DEFAULT_IMAGE_TYPE.toString()))
            .andExpect(jsonPath("$.imageContentType").value(DEFAULT_IMAGE_CONTENT_TYPE))
            .andExpect(jsonPath("$.image").value(Base64Utils.encodeToString(DEFAULT_IMAGE)))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deletedAt").value(sameInstant(DEFAULT_DELETED_AT)));
    }

    @Test
    @Transactional
    public void getNonExistingUserImage() throws Exception {
        // Get the userImage
        restUserImageMockMvc.perform(get("/api/user-images/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserImage() throws Exception {
        // Initialize the database
        userImageRepository.saveAndFlush(userImage);
        int databaseSizeBeforeUpdate = userImageRepository.findAll().size();

        // Update the userImage
        UserImage updatedUserImage = userImageRepository.findOne(userImage.getId());
        updatedUserImage
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .imageType(UPDATED_IMAGE_TYPE)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deletedAt(UPDATED_DELETED_AT);
        UserImageDTO userImageDTO = userImageMapper.toDto(updatedUserImage);

        restUserImageMockMvc.perform(put("/api/user-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userImageDTO)))
            .andExpect(status().isOk());

        // Validate the UserImage in the database
        List<UserImage> userImageList = userImageRepository.findAll();
        assertThat(userImageList).hasSize(databaseSizeBeforeUpdate);
        UserImage testUserImage = userImageList.get(userImageList.size() - 1);
        assertThat(testUserImage.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testUserImage.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testUserImage.getImageType()).isEqualTo(UPDATED_IMAGE_TYPE);
        assertThat(testUserImage.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testUserImage.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
        assertThat(testUserImage.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testUserImage.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testUserImage.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testUserImage.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testUserImage.getDeletedAt()).isEqualTo(UPDATED_DELETED_AT);
    }

    @Test
    @Transactional
    public void updateNonExistingUserImage() throws Exception {
        int databaseSizeBeforeUpdate = userImageRepository.findAll().size();

        // Create the UserImage
        UserImageDTO userImageDTO = userImageMapper.toDto(userImage);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUserImageMockMvc.perform(put("/api/user-images")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userImageDTO)))
            .andExpect(status().isCreated());

        // Validate the UserImage in the database
        List<UserImage> userImageList = userImageRepository.findAll();
        assertThat(userImageList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteUserImage() throws Exception {
        // Initialize the database
        userImageRepository.saveAndFlush(userImage);
        int databaseSizeBeforeDelete = userImageRepository.findAll().size();

        // Get the userImage
        restUserImageMockMvc.perform(delete("/api/user-images/{id}", userImage.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<UserImage> userImageList = userImageRepository.findAll();
        assertThat(userImageList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserImage.class);
        UserImage userImage1 = new UserImage();
        userImage1.setId(1L);
        UserImage userImage2 = new UserImage();
        userImage2.setId(userImage1.getId());
        assertThat(userImage1).isEqualTo(userImage2);
        userImage2.setId(2L);
        assertThat(userImage1).isNotEqualTo(userImage2);
        userImage1.setId(null);
        assertThat(userImage1).isNotEqualTo(userImage2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserImageDTO.class);
        UserImageDTO userImageDTO1 = new UserImageDTO();
        userImageDTO1.setId(1L);
        UserImageDTO userImageDTO2 = new UserImageDTO();
        assertThat(userImageDTO1).isNotEqualTo(userImageDTO2);
        userImageDTO2.setId(userImageDTO1.getId());
        assertThat(userImageDTO1).isEqualTo(userImageDTO2);
        userImageDTO2.setId(2L);
        assertThat(userImageDTO1).isNotEqualTo(userImageDTO2);
        userImageDTO1.setId(null);
        assertThat(userImageDTO1).isNotEqualTo(userImageDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(userImageMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(userImageMapper.fromId(null)).isNull();
    }
}
