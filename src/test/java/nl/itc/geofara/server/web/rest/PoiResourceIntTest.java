package nl.itc.geofara.server.web.rest;

import nl.itc.geofara.server.GeofaraApp;

import nl.itc.geofara.server.domain.Poi;
import nl.itc.geofara.server.repository.PoiRepository;
import nl.itc.geofara.server.service.PoiService;
import nl.itc.geofara.server.service.dto.PoiDTO;
import nl.itc.geofara.server.service.mapper.PoiMapper;
import nl.itc.geofara.server.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static nl.itc.geofara.server.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PoiResource REST controller.
 *
 * @see PoiResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GeofaraApp.class)
public class PoiResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_DELETED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DELETED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private PoiRepository poiRepository;

    @Autowired
    private PoiMapper poiMapper;

    @Autowired
    private PoiService poiService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPoiMockMvc;

    private Poi poi;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PoiResource poiResource = new PoiResource(poiService);
        this.restPoiMockMvc = MockMvcBuilders.standaloneSetup(poiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Poi createEntity(EntityManager em) {
        Poi poi = new Poi()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .latitude(DEFAULT_LATITUDE)
            .longitude(DEFAULT_LONGITUDE)
            .createdAt(DEFAULT_CREATED_AT)
            .updatedAt(DEFAULT_UPDATED_AT)
            .deletedAt(DEFAULT_DELETED_AT);
        return poi;
    }

    @Before
    public void initTest() {
        poi = createEntity(em);
    }

    @Test
    @Transactional
    public void createPoi() throws Exception {
        int databaseSizeBeforeCreate = poiRepository.findAll().size();

        // Create the Poi
        PoiDTO poiDTO = poiMapper.toDto(poi);
        restPoiMockMvc.perform(post("/api/pois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(poiDTO)))
            .andExpect(status().isCreated());

        // Validate the Poi in the database
        List<Poi> poiList = poiRepository.findAll();
        assertThat(poiList).hasSize(databaseSizeBeforeCreate + 1);
        Poi testPoi = poiList.get(poiList.size() - 1);
        assertThat(testPoi.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPoi.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPoi.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testPoi.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testPoi.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPoi.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testPoi.getDeletedAt()).isEqualTo(DEFAULT_DELETED_AT);
    }

    @Test
    @Transactional
    public void createPoiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = poiRepository.findAll().size();

        // Create the Poi with an existing ID
        poi.setId(1L);
        PoiDTO poiDTO = poiMapper.toDto(poi);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPoiMockMvc.perform(post("/api/pois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(poiDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Poi> poiList = poiRepository.findAll();
        assertThat(poiList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = poiRepository.findAll().size();
        // set the field null
        poi.setName(null);

        // Create the Poi, which fails.
        PoiDTO poiDTO = poiMapper.toDto(poi);

        restPoiMockMvc.perform(post("/api/pois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(poiDTO)))
            .andExpect(status().isBadRequest());

        List<Poi> poiList = poiRepository.findAll();
        assertThat(poiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLatitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = poiRepository.findAll().size();
        // set the field null
        poi.setLatitude(null);

        // Create the Poi, which fails.
        PoiDTO poiDTO = poiMapper.toDto(poi);

        restPoiMockMvc.perform(post("/api/pois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(poiDTO)))
            .andExpect(status().isBadRequest());

        List<Poi> poiList = poiRepository.findAll();
        assertThat(poiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLongitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = poiRepository.findAll().size();
        // set the field null
        poi.setLongitude(null);

        // Create the Poi, which fails.
        PoiDTO poiDTO = poiMapper.toDto(poi);

        restPoiMockMvc.perform(post("/api/pois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(poiDTO)))
            .andExpect(status().isBadRequest());

        List<Poi> poiList = poiRepository.findAll();
        assertThat(poiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = poiRepository.findAll().size();
        // set the field null
        poi.setCreatedAt(null);

        // Create the Poi, which fails.
        PoiDTO poiDTO = poiMapper.toDto(poi);

        restPoiMockMvc.perform(post("/api/pois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(poiDTO)))
            .andExpect(status().isBadRequest());

        List<Poi> poiList = poiRepository.findAll();
        assertThat(poiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUpdatedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = poiRepository.findAll().size();
        // set the field null
        poi.setUpdatedAt(null);

        // Create the Poi, which fails.
        PoiDTO poiDTO = poiMapper.toDto(poi);

        restPoiMockMvc.perform(post("/api/pois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(poiDTO)))
            .andExpect(status().isBadRequest());

        List<Poi> poiList = poiRepository.findAll();
        assertThat(poiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPois() throws Exception {
        // Initialize the database
        poiRepository.saveAndFlush(poi);

        // Get all the poiList
        restPoiMockMvc.perform(get("/api/pois?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(poi.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(sameInstant(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
            .andExpect(jsonPath("$.[*].deletedAt").value(hasItem(sameInstant(DEFAULT_DELETED_AT))));
    }

    @Test
    @Transactional
    public void getPoi() throws Exception {
        // Initialize the database
        poiRepository.saveAndFlush(poi);

        // Get the poi
        restPoiMockMvc.perform(get("/api/pois/{id}", poi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(poi.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.createdAt").value(sameInstant(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
            .andExpect(jsonPath("$.deletedAt").value(sameInstant(DEFAULT_DELETED_AT)));
    }

    @Test
    @Transactional
    public void getNonExistingPoi() throws Exception {
        // Get the poi
        restPoiMockMvc.perform(get("/api/pois/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePoi() throws Exception {
        // Initialize the database
        poiRepository.saveAndFlush(poi);
        int databaseSizeBeforeUpdate = poiRepository.findAll().size();

        // Update the poi
        Poi updatedPoi = poiRepository.findOne(poi.getId());
        updatedPoi
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .latitude(UPDATED_LATITUDE)
            .longitude(UPDATED_LONGITUDE)
            .createdAt(UPDATED_CREATED_AT)
            .updatedAt(UPDATED_UPDATED_AT)
            .deletedAt(UPDATED_DELETED_AT);
        PoiDTO poiDTO = poiMapper.toDto(updatedPoi);

        restPoiMockMvc.perform(put("/api/pois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(poiDTO)))
            .andExpect(status().isOk());

        // Validate the Poi in the database
        List<Poi> poiList = poiRepository.findAll();
        assertThat(poiList).hasSize(databaseSizeBeforeUpdate);
        Poi testPoi = poiList.get(poiList.size() - 1);
        assertThat(testPoi.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPoi.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPoi.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testPoi.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testPoi.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPoi.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testPoi.getDeletedAt()).isEqualTo(UPDATED_DELETED_AT);
    }

    @Test
    @Transactional
    public void updateNonExistingPoi() throws Exception {
        int databaseSizeBeforeUpdate = poiRepository.findAll().size();

        // Create the Poi
        PoiDTO poiDTO = poiMapper.toDto(poi);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPoiMockMvc.perform(put("/api/pois")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(poiDTO)))
            .andExpect(status().isCreated());

        // Validate the Poi in the database
        List<Poi> poiList = poiRepository.findAll();
        assertThat(poiList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePoi() throws Exception {
        // Initialize the database
        poiRepository.saveAndFlush(poi);
        int databaseSizeBeforeDelete = poiRepository.findAll().size();

        // Get the poi
        restPoiMockMvc.perform(delete("/api/pois/{id}", poi.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Poi> poiList = poiRepository.findAll();
        assertThat(poiList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Poi.class);
        Poi poi1 = new Poi();
        poi1.setId(1L);
        Poi poi2 = new Poi();
        poi2.setId(poi1.getId());
        assertThat(poi1).isEqualTo(poi2);
        poi2.setId(2L);
        assertThat(poi1).isNotEqualTo(poi2);
        poi1.setId(null);
        assertThat(poi1).isNotEqualTo(poi2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PoiDTO.class);
        PoiDTO poiDTO1 = new PoiDTO();
        poiDTO1.setId(1L);
        PoiDTO poiDTO2 = new PoiDTO();
        assertThat(poiDTO1).isNotEqualTo(poiDTO2);
        poiDTO2.setId(poiDTO1.getId());
        assertThat(poiDTO1).isEqualTo(poiDTO2);
        poiDTO2.setId(2L);
        assertThat(poiDTO1).isNotEqualTo(poiDTO2);
        poiDTO1.setId(null);
        assertThat(poiDTO1).isNotEqualTo(poiDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(poiMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(poiMapper.fromId(null)).isNull();
    }
}
